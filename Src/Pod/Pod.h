/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef PODOMATION_POD_H
#define PODOMATION_POD_H


#include "../Prereqs.h"
#include "../TypeDefs.h"
#include "../Defs.h"
#include "../State/State.h"


typedef LPFN_MODULE_DISPATCH_PROC	LPFN_POD_EFFECT_PROC;


typedef struct __PODPARAM
{	
	HANDLE 		hPod;
	HANDLE 		hEvent;
	HSTRING 	hsParam;
	LPVOID 		lpData;
	DLPVOID 	dlpUserData;
	DLPVOID		dlpCalleeData;
} PODPARAM, *LPPODPARAM;




typedef struct __PODREGISTER
{
	HANDLE 		hPod;
	HANDLE 		hEvent;
	HSTRING 	hsEvent;
	LPVOID 		lpParam;
	DLPVOID 	dlpUserData;
	DLPVOID 	dlpCalleeData;
} PODREGISTER, *LPPODREGISTER;




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODOMATION_API
HANDLE
CreatePod(
	_In_ 		LPCSTR 		lpcszModule,
	_In_Opt_ 	LPVOID 		lpData,
	_In_Opt_ 	ULONG 		ulFlags
);




_Success_(return != FALSE, _Interlocked_Operation_)
PODOMATION_API
BOOL
RegisterPodEvent(
	_In_ 		HANDLE 		hPod,
	_In_Z_ 		LPCSTR 		lpcszEvent,
	_In_Z_ 		LPCSTR 		lpcszDispatch,
	_In_Opt_	LPVOID 		lpParam
);




#endif
/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "Pod.h"

#define POD_CREATION_PARSE_BUFFER_SIZE		0x2000
#define POD_EVENT_HASH_TABLE_SIZE		0x2000

typedef struct __POD
{
	INHERITS_FROM_HANDLE();
	HANDLE 			hLock;
	
	HANDLE 			hConstructorModule;
	LPFN_POD_EFFECT_PROC	lpfnConstructorModule;
	LPPODPARAM		lpppConstructor;

	HHASHTABLE		hhtEvents;

} POD, *LPPOD;



typedef struct __POD_EVENT_INFO
{
	HANDLE 			hPod;
	HANDLE 			hModule;
	LPFN_POD_EFFECT_PROC	lpfnCallback;
	PODPARAM		ppInfo;
} POD_EVENT_INFO, *LPPOD_EVENT_INFO;




_Success_(return != FALSE, _Interlocked_Operation_)
INTERNAL_OPERATION
BOOL
__DestroyPod(
	_In_ 		HDERIVATIVE		hDerivative,
	_In_Opt_ 	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

	LPPOD lpPod;
	lpPod = (LPPOD)hDerivative;

	WaitForSingleObject(lpPod->hLock, INFINITE_WAIT_TIME);

	DestroyObject(lpPod->hConstructorModule);
	DestroyObject(lpPod->hLock);

	FreeMemory(lpPod);
	return TRUE;
}




/* 
	Helper procedure to get the info out of the
	Module:Procedure(PARAM(s)) format.

	The PARAM(s) that are returned, are still a
	STRING and need to be parsed further.
*/
_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__ParseModuleInfo(
	_In_ 		LPCSTR 			lpcszData,
	_Out_Z_		LPSTR 			lpszModule,
	_Out_Z_		LPSTR 			lpszProc,
	_Out_Z_ 	LPSTR 			lpszParam,
	_In_ 		UARCHLONG 		ualBufferSizes		
){
	DLPSTR dlpszSplit;
	UARCHLONG ualSize, ualCount;

	ZeroMemory(lpszModule, ualBufferSizes);	
	ZeroMemory(lpszProc, ualBufferSizes);
	ZeroMemory(lpszParam, ualBufferSizes);

	ualCount = StringSplit(lpcszData, ":(", &dlpszSplit);

	if (3 > ualCount)
	{ 
		DestroySplitString(dlpszSplit);
		return FALSE;
	}

	StringPrintFormatSafe(
		lpszModule,
		ualBufferSizes - 1,
		"%s%s",
		POD_MODULE_DIR,
		dlpszSplit[0]
	);

	StringCopySafe(lpszProc, dlpszSplit[1], ualBufferSizes);
	StringCopySafe(lpszParam, dlpszSplit[2], ualBufferSizes);

	DestroySplitString(dlpszSplit);

	ualSize = StringLength(lpszParam);
	if (')' == lpszParam[ualSize - 1])
	{ lpszParam[ualSize - 1] = '\0'; }

	StringReplaceCharacter(lpszParam, POD_SPACE_ESCAPE, ' ');
	return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODOMATION_API
HANDLE
CreatePod(
	_In_ 		LPCSTR 		lpcszModule,
	_In_Opt_ 	LPVOID 		lpData,
	_In_Opt_ 	ULONG 		ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(lpcszModule, NULL_OBJECT);

	HANDLE hPod;
	LPPOD lpPod;
	CSTRING csModule[POD_CREATION_PARSE_BUFFER_SIZE];
	CSTRING csProc[POD_CREATION_PARSE_BUFFER_SIZE];
	CSTRING csParam[POD_CREATION_PARSE_BUFFER_SIZE];

	__ParseModuleInfo(
		lpcszModule,
		csModule,
		csProc,
		csParam,
		POD_CREATION_PARSE_BUFFER_SIZE
	);


	/* Begin Allocation Of OBJECT */
	lpPod = GlobalAllocAndZero(sizeof(POD));

	hPod = CreateHandleWithSingleInheritor(
		lpPod,
		&(lpPod->hThis),
		HANDLE_TYPE_POD,
		__DestroyPod,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpPod->ullId),
		0
	);
	
	lpPod->lpppConstructor = GlobalAllocAndZero(sizeof(PODPARAM));
	lpPod->lpppConstructor->hsParam = CreateHeapString(csParam);
	lpPod->lpppConstructor->lpData = lpData;
	lpPod->lpppConstructor->hPod = hPod;
	lpPod->lpppConstructor->dlpUserData = GlobalAllocAndZero(sizeof(LPVOID));
	lpPod->lpppConstructor->dlpCalleeData = GlobalAllocAndZero(sizeof(LPVOID));


	lpPod->hConstructorModule = LoadModule(
		csModule,
		NULLPTR,
		NULLPTR,
		LOCK_TYPE_CRITICAL_SECTION,
		0xC00,
		lpPod->lpppConstructor
	);

	if (NULL_OBJECT == lpPod->hConstructorModule)
	{
		CSTRING csTemp[8192];
		ZeroMemory(csTemp, sizeof(csTemp));

		StringPrintFormatSafe(
			csTemp,
			sizeof(csTemp) - 1,
			"Module %s does not exist!\n",
			csModule
		);

		WriteLog(
			GetState()->hLog,
			csTemp,
			LOG_ERROR,
			0
		);

		FreeMemory(lpPod);
		return NULL_OBJECT;
	}

	lpPod->lpfnConstructorModule = GetModuleProcByName(
		lpPod->hConstructorModule,
		csProc
	);

	if (NULLPTR == lpPod->lpfnConstructorModule)
	{
		DestroyHeapString(lpPod->lpppConstructor->hsParam);
		FreeMemory(lpPod->lpppConstructor);
		DestroyObject(hPod);
		return NULL_OBJECT;
	}

	/* Create Event Hash Table */
	lpPod->hhtEvents = CreateHashTable(POD_EVENT_HASH_TABLE_SIZE);
	
	if (NULLPTR == lpPod->hhtEvents)
	{
		DestroyHeapString(lpPod->lpppConstructor->hsParam);
		FreeMemory(lpPod->lpppConstructor);
		DestroyObject(hPod);
		return NULL_OBJECT;
	}


	/* Init The POD */
	lpPod->lpfnConstructorModule(
		lpPod->hConstructorModule,
		lpPod->lpppConstructor
	);


	return hPod;
}




_Success_(return != FALSE, _Interlocked_Operation_)
PODOMATION_API
BOOL
RegisterPodEvent(
	_In_ 		HANDLE 		hPod,
	_In_Z_ 		LPCSTR 		lpcszEvent,
	_In_Z_ 		LPCSTR 		lpcszDispatch,
	_In_Opt_	LPVOID 		lpParam
){
	EXIT_IF_UNLIKELY_NULL(hPod, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszEvent, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszDispatch, FALSE);

	LPPOD lpPod;
	LPPOD_EVENT_INFO lppeiInfo;
	LPPODREGISTER lpprInfo;
	CSTRING csModule[POD_CREATION_PARSE_BUFFER_SIZE];
	CSTRING csProc[POD_CREATION_PARSE_BUFFER_SIZE];
	CSTRING csParam[POD_CREATION_PARSE_BUFFER_SIZE];

	lpPod = OBJECT_CAST(hPod, LPPOD);
	lppeiInfo = GlobalAllocAndZero(sizeof(POD_EVENT_INFO));
	
	__ParseModuleInfo(
		lpcszDispatch,
		csModule,
		csProc,
		csParam,
		POD_CREATION_PARSE_BUFFER_SIZE
	);

	lppeiInfo->hPod = hPod;
	lppeiInfo->hModule = LoadModule(
		csModule,
		NULLPTR,
		NULLPTR,
		LOCK_TYPE_CRITICAL_SECTION,
		0xC00,
		NULLPTR
	);

	/* Do Error Checks */
	if (NULL_OBJECT == lppeiInfo->hModule)
	{
		FreeMemory(lppeiInfo);
		return FALSE;
	}
	
	
	lppeiInfo->lpfnCallback = GetModuleProcByName(
		lppeiInfo->hModule,
		csProc
	);

	if (NULLPTR == lppeiInfo->lpfnCallback)
	{
		CSTRING csRetry[8192];
		ZeroMemory(csRetry, sizeof(csRetry));

		StringPrintFormatSafe(
			csRetry,
			sizeof(csRetry) - 1,
			"__%s", 
			csProc
		);
	
		lppeiInfo->lpfnCallback = GetModuleProcByName(
			lppeiInfo->hModule,
			csRetry
		);

		if (NULLPTR == lppeiInfo->lpfnCallback)
		{
			DestroyObject(lppeiInfo->hModule);
			FreeMemory(lppeiInfo);
			return FALSE;
		}
	}

	lppeiInfo->ppInfo.hPod = hPod;
	lppeiInfo->ppInfo.hsParam = CreateHeapString(csParam);
	lppeiInfo->ppInfo.lpData = lpParam;
	lppeiInfo->ppInfo.dlpUserData = lpPod->lpppConstructor->dlpUserData;
	lppeiInfo->ppInfo.dlpCalleeData = lpPod->lpppConstructor->dlpCalleeData;
	lppeiInfo->ppInfo.hEvent = CreateEventEx(
		FALSE,
		(LPFN_EVENT_CALLBACK_PROC)lppeiInfo->lpfnCallback,
		&(lppeiInfo->ppInfo)
	);


	HashTableInsertEx(
		lpPod->hhtEvents,
		(ULONGLONG)(LPSTR)lpcszEvent,
		StringLength(lpcszEvent),
		FALSE,
		(ULONGLONG)lppeiInfo
	);


	/* Register The Event */
	lpprInfo = GlobalAllocAndZero(sizeof(PODREGISTER));
	lpprInfo->hPod = hPod;
	lpprInfo->hEvent = lppeiInfo->ppInfo.hEvent;
	lpprInfo->hsEvent = CreateHeapString(lpcszEvent);
	lpprInfo->lpParam = lpParam;
	lpprInfo->dlpUserData = lppeiInfo->ppInfo.dlpUserData;
	lpprInfo->dlpCalleeData = lppeiInfo->ppInfo.dlpCalleeData;


	CallModuleProcByName(
		lpPod->hConstructorModule,
		"RegisterEvent",
		lpprInfo
	);

	return TRUE;
}
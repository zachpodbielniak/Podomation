/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>



typedef struct __PUBLIC_IP_EVENT
{
	IPV4_ADDRESS		ip4Address;
	IPV6_ADDRESS		ip6Address;
	HANDLE 			hThread;
	HANDLE 			hProcess;

	HVECTOR 		hvOnExpected;
	HVECTOR 		hvOnDifferent;
	HVECTOR 		hvOnChange;
	HVECTOR 		hvOnInitialDifferent;
	HVECTOR 		hvOnInitialExpected;

	UARCHLONG 		ualWaitTime;
	BOOL 			bExpected;
	BOOL 			bInitialDifference;
	BOOL 			bInitialExpected;
	BOOL 			bV4;
} PUBLIC_IP_EVENT, *LPPUBLIC_IP_EVENT, **DLPPUBLIC_IP_EVENT;




PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 			hModule,
	_In_Opt_ 	LPVOID 			lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded PublicIpEvent Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}



INTERNAL_OPERATION
VOID
__V4(
	_In_ 		LPPUBLIC_IP_EVENT	lppipeInfo
){

	IPV4_ADDRESS ip4Address;
	UARCHLONG ualIndex;

	ip4Address = GetCurrentPublicIpv4Address();

	/* Expected */
	if (lppipeInfo->ip4Address.ulData == ip4Address.ulData)
	{
		/* Check For Change */
		if (FALSE == lppipeInfo->bExpected)
		{
			for (
				ualIndex = 0;
				ualIndex < VectorSize(lppipeInfo->hvOnChange);
				ualIndex++
			){
				LPPODPARAM lpX;
				lpX = *(LPPODPARAM *)VectorAt(lppipeInfo->hvOnChange, ualIndex);

				if (NULLPTR != lpX)
				{
					SignalEvent(lpX->hEvent);
					ResetEvent(lpX->hEvent);
				}
			}
		}
		

		/* Check For Initial Expected */
		if (FALSE == lppipeInfo->bInitialExpected)
		{
			for (
				ualIndex = 0;
				ualIndex < VectorSize(lppipeInfo->hvOnInitialExpected);
				ualIndex++
			){
				LPPODPARAM lpX;
				lpX = *(LPPODPARAM *)VectorAt(lppipeInfo->hvOnInitialExpected, ualIndex);

				if (NULLPTR != lpX)
				{
					SignalEvent(lpX->hEvent);
					ResetEvent(lpX->hEvent);
				}
			}
		}
		
		lppipeInfo->bExpected = TRUE;
		lppipeInfo->bInitialExpected = TRUE;
		lppipeInfo->bInitialDifference = FALSE;

		for (
			ualIndex = 0;
			ualIndex < VectorSize(lppipeInfo->hvOnExpected);
			ualIndex++
		){
			LPPODPARAM lpX;
			lpX = *(LPPODPARAM *)VectorAt(lppipeInfo->hvOnExpected, ualIndex);

			if (NULLPTR != lpX)
			{
				SignalEvent(lpX->hEvent);
				ResetEvent(lpX->hEvent);
			}
		}
	}
	/* Different */
	else
	{
		/* Check For Change */
		if (FALSE != lppipeInfo->bExpected)
		{
			for (
				ualIndex = 0;
				ualIndex < VectorSize(lppipeInfo->hvOnChange);
				ualIndex++
			){
				LPPODPARAM lpX;
				lpX = *(LPPODPARAM *)VectorAt(lppipeInfo->hvOnChange, ualIndex);

				if (NULLPTR != lpX)
				{
					SignalEvent(lpX->hEvent);
					ResetEvent(lpX->hEvent);
				}
			}
		}
		
		/* Check For Initial Difference */
		if (FALSE == lppipeInfo->bInitialDifference)
		{
			for (
				ualIndex = 0;
				ualIndex < VectorSize(lppipeInfo->hvOnInitialDifferent);
				ualIndex++
			){
				LPPODPARAM lpX;
				lpX = *(LPPODPARAM *)VectorAt(lppipeInfo->hvOnInitialDifferent, ualIndex);

				if (NULLPTR != lpX)
				{
					SignalEvent(lpX->hEvent);
					ResetEvent(lpX->hEvent);
				}
			}
		}

		lppipeInfo->bExpected = FALSE;
		lppipeInfo->bInitialDifference = TRUE;
		lppipeInfo->bInitialExpected = FALSE;

		/* OnDifferent */
		for (
			ualIndex = 0;
			ualIndex < VectorSize(lppipeInfo->hvOnDifferent);
			ualIndex++
		){
			LPPODPARAM lpX;
			lpX = *(LPPODPARAM *)VectorAt(lppipeInfo->hvOnDifferent, ualIndex);

			if (NULLPTR != lpX)
			{
				SignalEvent(lpX->hEvent);
				ResetEvent(lpX->hEvent);
			}
		}
	}
}




INTERNAL_OPERATION
VOID
__V6(
	_In_ 		LPPUBLIC_IP_EVENT	lppipeInfo
){

	LPIPV6_ADDRESS lpip6Address;
	UARCHLONG ualIndex;

	lpip6Address = GetCurrentPublicIpv6Address();

	/* Expected */
	if (0 == CompareMemory(
		lpip6Address,
		&(lppipeInfo->ip6Address),
		sizeof(IPV6_ADDRESS)
	)){
		/* Check For Change */
		if (FALSE == lppipeInfo->bExpected)
		{
			for (
				ualIndex = 0;
				ualIndex < VectorSize(lppipeInfo->hvOnChange);
				ualIndex++
			){
				LPPODPARAM lpX;
				lpX = *(LPPODPARAM *)VectorAt(lppipeInfo->hvOnChange, ualIndex);

				if (NULLPTR != lpX)
				{
					SignalEvent(lpX->hEvent);
					ResetEvent(lpX->hEvent);
				}
			}
		}
		

		/* Check For Initial Expected */
		if (FALSE == lppipeInfo->bInitialExpected)
		{
			for (
				ualIndex = 0;
				ualIndex < VectorSize(lppipeInfo->hvOnInitialExpected);
				ualIndex++
			){
				LPPODPARAM lpX;
				lpX = *(LPPODPARAM *)VectorAt(lppipeInfo->hvOnInitialExpected, ualIndex);

				if (NULLPTR != lpX)
				{
					SignalEvent(lpX->hEvent);
					ResetEvent(lpX->hEvent);
				}
			}
		}
		
		lppipeInfo->bExpected = TRUE;
		lppipeInfo->bInitialExpected = TRUE;
		lppipeInfo->bInitialDifference = FALSE;

		for (
			ualIndex = 0;
			ualIndex < VectorSize(lppipeInfo->hvOnExpected);
			ualIndex++
		){
			LPPODPARAM lpX;
			lpX = *(LPPODPARAM *)VectorAt(lppipeInfo->hvOnExpected, ualIndex);

			if (NULLPTR != lpX)
			{
				SignalEvent(lpX->hEvent);
				ResetEvent(lpX->hEvent);
			}
		}
	}
	/* Different */
	else
	{
		/* Check For Change */
		if (FALSE != lppipeInfo->bExpected)
		{
			for (
				ualIndex = 0;
				ualIndex < VectorSize(lppipeInfo->hvOnChange);
				ualIndex++
			){
				LPPODPARAM lpX;
				lpX = *(LPPODPARAM *)VectorAt(lppipeInfo->hvOnChange, ualIndex);

				if (NULLPTR != lpX)
				{
					SignalEvent(lpX->hEvent);
					ResetEvent(lpX->hEvent);
				}
			}
		}
		
		/* Check For Initial Difference */
		if (FALSE == lppipeInfo->bInitialDifference)
		{
			for (
				ualIndex = 0;
				ualIndex < VectorSize(lppipeInfo->hvOnInitialDifferent);
				ualIndex++
			){
				LPPODPARAM lpX;
				lpX = *(LPPODPARAM *)VectorAt(lppipeInfo->hvOnInitialDifferent, ualIndex);

				if (NULLPTR != lpX)
				{
					SignalEvent(lpX->hEvent);
					ResetEvent(lpX->hEvent);
				}
			}
		}

		lppipeInfo->bExpected = FALSE;
		lppipeInfo->bInitialDifference = TRUE;
		lppipeInfo->bInitialExpected = FALSE;

		/* OnDifferent */
		for (
			ualIndex = 0;
			ualIndex < VectorSize(lppipeInfo->hvOnDifferent);
			ualIndex++
		){
			LPPODPARAM lpX;
			lpX = *(LPPODPARAM *)VectorAt(lppipeInfo->hvOnDifferent, ualIndex);

			if (NULLPTR != lpX)
			{
				SignalEvent(lpX->hEvent);
				ResetEvent(lpX->hEvent);
			}
		}
	}
}




PODOMATION_MODULE
LPVOID
__Proc(
	_In_Opt_	LPVOID 			lpParam
){
	LPPODPARAM lpppInfo;
	LPPUBLIC_IP_EVENT lppipeInfo;

	lpppInfo = (LPPODPARAM)lpParam;
	lppipeInfo = *(lpppInfo->dlpUserData);

	INFINITE_LOOP()
	{
		/* IPv6 */
		if (FALSE == lppipeInfo->bV4)
		{ __V6(lppipeInfo); }
		/* IPv4 */
		else
		{ __V4(lppipeInfo); }

		Sleep(lppipeInfo->ualWaitTime);
	}
}



PODOMATION_MODULE
LPVOID
RegisterEvent(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODREGISTER lpprInfo;
	LPPUBLIC_IP_EVENT lppipeInfo;

	lpprInfo = (LPPODREGISTER)lpParam;
	lppipeInfo = *(lpprInfo->dlpUserData);

	/* Parse Type */
	if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnExpected"
	)){ VectorPushBack(lppipeInfo->hvOnExpected, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnDifferent"
	)){ VectorPushBack(lppipeInfo->hvOnDifferent, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnChange"
	)){ VectorPushBack(lppipeInfo->hvOnChange, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnInitialExpected"
	)){ VectorPushBack(lppipeInfo->hvOnInitialExpected, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnInitialDifferent"
	)){ VectorPushBack(lppipeInfo->hvOnInitialDifferent, &lpprInfo, 0); }
	else
	{
		CSTRING csBuffer[8192];
		ZeroMemory(csBuffer, sizeof(csBuffer));

		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			"%s is not a valid PublicEvent!\n",
			HeapStringValue(lpprInfo->hsEvent)
		);

		WriteFile(GetStandardError(), ANSI_RED, 0);
		WriteFile(GetStandardError(), csBuffer, 0);
		WriteFile(GetStandardError(), ANSI_RESET, 0);

		WriteLog(
			GetState()->hLog,
			csBuffer,
			LOG_FATAL,
			0
		);

		PostQuitMessage(1);
	}
	return NULLPTR;
}



PODOMATION_MODULE
LPVOID 
New(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODPARAM lpppPod;
	LPPUBLIC_IP_EVENT lppipeInfo;
	DLPSTR dlpszSplit;
	UARCHLONG ualCount;

	lpppPod = (LPPODPARAM)lpParam;
	lppipeInfo = GlobalAllocAndZero(sizeof(PUBLIC_IP_EVENT));

	ualCount = StringSplit(
		HeapStringValue(lpppPod->hsParam),
		",",
		&dlpszSplit
	);

	if (2 != ualCount)
	{
		WriteFile(GetStandardOut(), ANSI_RED "Expected two arguments to PublicIpEvent:New(<IP>,<WAIT_TIME>)\n" ANSI_RESET, 0);
		DestroyObject(GetState()->hLog);
		PostQuitMessage(2);
	}

	if ('\0' != dlpszSplit[0][0])
	{
		/* IPV4 */
		if (StringInString(dlpszSplit[0], "."))
		{ 
			lppipeInfo->bV4 = TRUE;
			lppipeInfo->ip4Address = CreateIpv4AddressFromString(dlpszSplit[0]);
		}
		else
		{ 
			lppipeInfo->bV4 = FALSE;
			InitializeIpv6Address(&(lppipeInfo->ip6Address), dlpszSplit[0]); 
		}
	}

	StringScanFormat(dlpszSplit[1], "%lu", &lppipeInfo->ualWaitTime);
	
	lppipeInfo->hvOnDifferent = CreateVector(16, sizeof(LPPUBLIC_IP_EVENT), 0);
	lppipeInfo->hvOnInitialDifferent = CreateVector(16, sizeof(LPPUBLIC_IP_EVENT), 0);
	lppipeInfo->hvOnInitialExpected = CreateVector(16, sizeof(LPPUBLIC_IP_EVENT), 0);
	lppipeInfo->hvOnExpected = CreateVector(16, sizeof(LPPUBLIC_IP_EVENT), 0);
	lppipeInfo->hvOnChange = CreateVector(16, sizeof(LPPUBLIC_IP_EVENT), 0);

	lppipeInfo->bExpected = TRUE;
	lppipeInfo->bInitialExpected = TRUE;

	*(lpppPod->dlpUserData) = (LPVOID)lppipeInfo;

	lppipeInfo->hThread = CreateThread(__Proc, lpppPod, NULLPTR);
	return NULLPTR;
}
/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>


typedef struct __PROCESS_EVENT
{
	HSTRING 		hsProgram;
	HSTRING 		hsArgs;
	HANDLE 			hThread;
	HANDLE 			hProcess;
	LPPODREGISTER 		lpprOnSuccess;
	LPPODREGISTER 		lpprOnError;
} PROCESS_EVENT, *LPPROCESS_EVENT;







PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded TimerEvent Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}



PODOMATION_MODULE
LPVOID
__Proc(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	LPPROCESS_EVENT lppeInfo;
	LONG lStatus;

	lpppInfo = (LPPODPARAM)lpParam;
	lppeInfo = *(lpppInfo->dlpUserData);

	Sleep(1000);

	INFINITE_LOOP()
	{
		lppeInfo->hProcess = NULL_OBJECT;
		lppeInfo->hProcess = CreateProcess(
			HeapStringValue(lppeInfo->hsProgram),
			HeapStringValue(lppeInfo->hsArgs),
			FALSE
		);

		if (NULL_OBJECT == lppeInfo->hProcess)
		{
			PostQuitMessage(2);
		}

		WaitForSingleObject(lppeInfo->hProcess, INFINITE_WAIT_TIME);
		lStatus = GetProcessReturnStatus(lppeInfo->hProcess);

		*(lpppInfo->dlpCalleeData) = GetProcessOutput(lppeInfo->hProcess);

		if (0 == lStatus)
		{
			if (NULLPTR != lppeInfo->lpprOnSuccess)
			{
				SignalEvent(lppeInfo->lpprOnSuccess->hEvent);
				ResetEvent(lppeInfo->lpprOnSuccess->hEvent);
			}
		}
		else 
		{
			if (NULLPTR != lppeInfo->lpprOnError)
			{
				SignalEvent(lppeInfo->lpprOnError->hEvent);
				ResetEvent(lppeInfo->lpprOnError->hEvent);
			}
		}
		Sleep(20);
		DestroyObject(lppeInfo->hProcess);
	}
}





PODOMATION_MODULE
LPVOID
RegisterEvent(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODREGISTER lpprInfo;
	LPPROCESS_EVENT lppeInfo;

	lpprInfo = (LPPODREGISTER)lpParam;
	lppeInfo = *(lpprInfo->dlpUserData);

	/* Parse Type */
	if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnSuccess"
	)){ lppeInfo->lpprOnSuccess = lpprInfo; }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnError"
	)){ lppeInfo->lpprOnError = lpprInfo; }
	else
	{
		CSTRING csBuffer[8192];
		ZeroMemory(csBuffer, sizeof(csBuffer));

		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			"%s is not a valid ProcessEvent!\n",
			HeapStringValue(lpprInfo->hsEvent)
		);

		WriteFile(GetStandardError(), ANSI_RED, 0);
		WriteFile(GetStandardError(), csBuffer, 0);
		WriteFile(GetStandardError(), ANSI_RESET, 0);

		WriteLog(
			GetState()->hLog,
			csBuffer,
			LOG_FATAL,
			0
		);

		PostQuitMessage(1);
	}
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID 
New(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODPARAM lpppPod;
	LPPROCESS_EVENT lppeInfo;
	DLPSTR dlpszSplit;
	UARCHLONG ualSize, ualIndex;
	CSTRING csArgs[8192];

	lpppPod = (LPPODPARAM)lpParam;
	lppeInfo = GlobalAllocAndZero(sizeof(PROCESS_EVENT));

	ualSize = StringSplit(
		HeapStringValue(lpppPod->hsParam),
		",",
		&dlpszSplit
	);

	lppeInfo->hsProgram = CreateHeapString(dlpszSplit[0]);
	ZeroMemory(csArgs, sizeof(csArgs));


	if (2 < ualSize)
	{
		for (
			ualIndex = 1;
			ualIndex < ualSize;
			ualIndex++
		){ 
			if (1 != ualIndex)
			{ StringConcatenateSafe(csArgs, " ", sizeof(csArgs) - 1); }
			StringConcatenateSafe(csArgs, dlpszSplit[ualIndex], sizeof(csArgs) - 1);
		}

		lppeInfo->hsArgs = CreateHeapString(csArgs);
	}
	else if (1 < ualSize)
	{ lppeInfo->hsArgs = CreateHeapString(dlpszSplit[1]); }
	else
	{
		/* TODO: Handle Error */
	}

	*(lpppPod->dlpUserData) = (LPVOID)lppeInfo;

	lppeInfo->hThread = CreateThread(__Proc, lpppPod, NULLPTR);
	return NULLPTR;
}
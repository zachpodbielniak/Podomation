/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>
#include <mpd/client.h>




#define MPC_BASIC_COMMAND_BODY(CMD)							\
	LPPODPARAM lpppInfo;								\
	USHORT usPort;									\
	DLPSTR dlpszSplit;								\
	UARCHLONG ualCount;								\
	struct mpd_connection *mcConn;							\
											\
											\
	lpppInfo = (LPPODPARAM)lpParam;							\
	usPort = 0;									\
											\
	ualCount = StringSplit(								\
		HeapStringValue(lpppInfo->hsParam),					\
		",",									\
		&dlpszSplit								\
	);										\
											\
	if (2 != ualCount)								\
	{										\
		WriteFile(GetStandardOut(), "Expecting two parameters.\n", 0);		\
		PostQuitMessage(2);							\
	}										\
											\
	StringScanFormat(								\
		dlpszSplit[1],								\
		"%hu",									\
		&usPort									\
	);										\
											\
	mcConn = mpd_connection_new(							\
		dlpszSplit[0],								\
		usPort,									\
		1000									\
	);										\
											\
	mpd_send_command(mcConn, (CMD), NULL);						\
	mpd_connection_free(mcConn);							\
	DestroySplitString(dlpszSplit);							\





PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded Mpd Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}






PODOMATION_MODULE
LPVOID
Pause(
	_In_Opt_ 	LPVOID 		lpParam
){
	MPC_BASIC_COMMAND_BODY("pause");
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
Play(
	_In_Opt_ 	LPVOID 		lpParam
){
	MPC_BASIC_COMMAND_BODY("play");
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
Stop(
	_In_Opt_ 	LPVOID 		lpParam
){
	MPC_BASIC_COMMAND_BODY("stop");
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
SetVolume(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;								
	USHORT usPort;									
	ULONG ulVolume;
	DLPSTR dlpszSplit;								
	UARCHLONG ualCount;
	struct mpd_connection *mcConn;							
											
											
	lpppInfo = (LPPODPARAM)lpParam;							
	usPort = 0;									
											
	ualCount = StringSplit(								
		HeapStringValue(lpppInfo->hsParam),					
		",",									
		&dlpszSplit								
	);										
											
	if (3 != ualCount)								
	{										
		WriteFile(GetStandardOut(), "Expecting two parameters.\n", 0);		
		PostQuitMessage(2);							
	}										
											
	StringScanFormat(								
		dlpszSplit[1],								
		"%hu",									
		&usPort									
	);

	StringScanFormat(
		dlpszSplit[2],
		"%u",
		&ulVolume
	);
											
	mcConn = mpd_connection_new(							
		dlpszSplit[0],								
		usPort,									
		1000									
	);										
											
	mpd_send_set_volume(mcConn, ulVolume);
	mpd_connection_free(mcConn);							
	DestroySplitString(dlpszSplit);							

	return NULLPTR;
}




/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <PodNet/CClock/CClock.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>



typedef struct __CLOCK_EVENT
{
	HANDLE 			hThread;

	HVECTOR 		hvOnTime;
	HVECTOR 		hvOnReboot;
	HVECTOR 		hvMinutely;
	HVECTOR 		hvHourly;
	HVECTOR 		hvDaily;
	HVECTOR 		hvWeekly;
	HVECTOR 		hvMonthly;
	HVECTOR 		hvYearly;
} CLOCK_EVENT, *LPCLOCK_EVENT;




PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded ClockEvent Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}



PODOMATION_MODULE
LPVOID
__ClockProc(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	LPCLOCK_EVENT lpceInfo;
	UARCHLONG ualIndex;
	UARCHLONG ualDiff;
	LPPODREGISTER lpX;
	struct tm *lptmT;
	time_t tRaw;

	lpppInfo = (LPPODPARAM)lpParam;
	lpceInfo = *(lpppInfo->dlpUserData);

	WaitForSingleObject(GetState()->hReady, INFINITE_WAIT_TIME);

	/* Reboot */
	for (
		ualIndex = 0;
		ualIndex < VectorSize(lpceInfo->hvOnReboot);
		ualIndex++
	){
		lpX = *(LPPODREGISTER *)VectorAt(lpceInfo->hvOnReboot, ualIndex);

		if (NULLPTR != lpX)
		{
			SignalEvent(lpX->hEvent);
			ResetEvent(lpX->hEvent);
		}
	}
	
	time(&tRaw);
	lptmT = localtime(&tRaw);
	LongSleep(60 - lptmT->tm_sec);

	INFINITE_LOOP()
	{
		time(&tRaw);
		lptmT = localtime(&tRaw);

		/* Minutely */
		for (
			ualIndex = 0;
			ualIndex < VectorSize(lpceInfo->hvMinutely);
			ualIndex++
		){
			lpX = *(LPPODREGISTER *)VectorAt(lpceInfo->hvMinutely, ualIndex);

			if (NULLPTR != lpX)
			{
				SignalEvent(lpX->hEvent);
				ResetEvent(lpX->hEvent);
			}
		}


		/* Hourly */
		if (0 == lptmT->tm_min)	
		{
			for (
				ualIndex = 0;
				ualIndex < VectorSize(lpceInfo->hvHourly);
				ualIndex++
			){
				lpX = *(LPPODREGISTER *)VectorAt(lpceInfo->hvHourly, ualIndex);

				if (NULLPTR != lpX)
				{
					SignalEvent(lpX->hEvent);
					ResetEvent(lpX->hEvent);
				}
			}
		}

		/* Daily */
		if (
			0 == lptmT->tm_min && 
			0 == lptmT->tm_hour
		){
			for (
				ualIndex = 0;
				ualIndex < VectorSize(lpceInfo->hvDaily);
				ualIndex++
			){
				lpX = *(LPPODREGISTER *)VectorAt(lpceInfo->hvDaily, ualIndex);

				if (NULLPTR != lpX)
				{
					SignalEvent(lpX->hEvent);
					ResetEvent(lpX->hEvent);
				}
			}
		}

		/* Weekly */
		if (
			0 == lptmT->tm_wday &&
			0 == lptmT->tm_hour &&
			0 == lptmT->tm_min
		){
			for (
				ualIndex = 0;
				ualIndex < VectorSize(lpceInfo->hvWeekly);
				ualIndex++
			){
				lpX = *(LPPODREGISTER *)VectorAt(lpceInfo->hvWeekly, ualIndex);

				if (NULLPTR != lpX)
				{
					SignalEvent(lpX->hEvent);
					ResetEvent(lpX->hEvent);
				}
			}
		}

		/* Monthly */
		if (
			1 == lptmT->tm_mday &&
			0 == lptmT->tm_wday &&
			0 == lptmT->tm_hour &&
			0 == lptmT->tm_min
		){
			for (
				ualIndex = 0;
				ualIndex < VectorSize(lpceInfo->hvMonthly);
				ualIndex++
			){
				lpX = *(LPPODREGISTER *)VectorAt(lpceInfo->hvMonthly, ualIndex);

				if (NULLPTR != lpX)
				{
					SignalEvent(lpX->hEvent);
					ResetEvent(lpX->hEvent);
				}
			}
		}

		/* Yearly */
		if (
			0 == lptmT->tm_yday &&
			1 == lptmT->tm_mday &&
			0 == lptmT->tm_wday &&
			0 == lptmT->tm_hour &&
			0 == lptmT->tm_min
		){
			for (
				ualIndex = 0;
				ualIndex < VectorSize(lpceInfo->hvYearly);
				ualIndex++
			){
				lpX = *(LPPODREGISTER *)VectorAt(lpceInfo->hvYearly, ualIndex);

				if (NULLPTR != lpX)
				{
					SignalEvent(lpX->hEvent);
					ResetEvent(lpX->hEvent);
				}
			}
		}

		/* OnTime */
		for (
			ualIndex = 0;
			ualIndex < VectorSize(lpceInfo->hvOnTime);
			ualIndex++
		){
			lpX = *(LPPODREGISTER *)VectorAt(lpceInfo->hvOnTime, ualIndex);
			
			if (NULLPTR != lpX)
			{
				/* Compare Times */
				struct tm *lptmEvent;
				lptmEvent = lpX->lpParam;

				if (
					(-1 == lptmEvent->tm_min || lptmEvent->tm_min == lptmT->tm_min) &&
					(-1 == lptmEvent->tm_hour || lptmEvent->tm_hour == lptmT->tm_hour) &&
					(-1 == lptmEvent->tm_wday || lptmEvent->tm_wday == lptmT->tm_wday) &&
					(-1 == lptmEvent->tm_mday || lptmEvent->tm_mday == lptmT->tm_mday) &&
					(-1 == lptmEvent->tm_mon || lptmEvent->tm_mon == lptmT->tm_mon) &&
					(-1 == lptmEvent->tm_yday || lptmEvent->tm_yday == lptmT->tm_yday)
				){
					SignalEvent(lpX->hEvent);
					ResetEvent(lpX->hEvent);
				}
			}
		}


		/* Run Once A Minute */
		time(&tRaw);
		lptmT = localtime(&tRaw);
		LongSleep(60 - lptmT->tm_sec);
	}
}




PODOMATION_MODULE
LPVOID
RegisterEvent(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODREGISTER lpprInfo;
	LPCLOCK_EVENT lpceInfo;

	lpprInfo = (LPPODREGISTER)lpParam;
	lpceInfo = *(lpprInfo->dlpUserData);

	/* Parse Type */
	if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnReboot"
	)){ VectorPushBack(lpceInfo->hvOnReboot, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnTheMinute"
	)){ VectorPushBack(lpceInfo->hvMinutely, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnTheHour"
	)){ VectorPushBack(lpceInfo->hvHourly, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnTheDay"
	)){ VectorPushBack(lpceInfo->hvDaily, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnTheWeek"
	)){ VectorPushBack(lpceInfo->hvWeekly, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnTheMonth"
	)){ VectorPushBack(lpceInfo->hvMonthly, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnTheYear"
	)){ VectorPushBack(lpceInfo->hvYearly, &lpprInfo, 0); }
	else if (0 != StringInString(
		HeapStringValue(lpprInfo->hsEvent),
		"OnTime-"
	)){
		/* Parse Time */
		DLPSTR dlpszSplit;
		UARCHLONG ualCount;
		struct tm *lptmX;
		lptmX = GlobalAllocAndZero(sizeof(struct tm));

		ualCount = StringSplit(
			(LPSTR)(HeapStringValue(lpprInfo->hsEvent) + StringLength("OnTime-")),
			"-",
			&dlpszSplit
		);
		
		if (6 != ualCount)
		{
			WriteFile(GetStandardOut(), "Expected 6 values for OnTime event.\n", 0);
			DestroyObject(GetState()->hLog);
			PostQuitMessage(2);
		}

		/* Min - Hour - Day - Month - Day Of Week - Day Of Year */

		/* Min */
		if (
			0 == StringCompare("Any", dlpszSplit[0]) ||
			0 == StringCompare("*", dlpszSplit[0])
		){ lptmX->tm_min = -1; }
		else
		{
			StringScanFormat(
				dlpszSplit[0],
				"%d",
				&(lptmX->tm_min)
			);
		}
		
		/* Hour */
		if (
			0 == StringCompare("Any", dlpszSplit[1]) ||
			0 == StringCompare("*", dlpszSplit[1])
		){ lptmX->tm_hour = -1; }
		else
		{
			StringScanFormat(
				dlpszSplit[1],
				"%d",
				&(lptmX->tm_hour)
			);
		}

		/* Day */
		if (
			0 == StringCompare("Any", dlpszSplit[2]) ||
			0 == StringCompare("*", dlpszSplit[2])
		){ lptmX->tm_mday = -1; }
		else
		{
			StringScanFormat(
				dlpszSplit[2],
				"%d",
				&(lptmX->tm_mday)
			);
		}
		
		/* Month */
		if (
			0 == StringCompare("Any", dlpszSplit[3]) ||
			0 == StringCompare("*", dlpszSplit[3])
		){ lptmX->tm_mon = -1; }
		else
		{
			StringScanFormat(
				dlpszSplit[3],
				"%d",
				&(lptmX->tm_mon)
			);
		}

		/* Day Of Week */
		if (
			0 == StringCompare("Any", dlpszSplit[4]) ||
			0 == StringCompare("*", dlpszSplit[4])
		){ lptmX->tm_wday = -1; }
		else
		{
			StringScanFormat(
				dlpszSplit[4],
				"%d",
				&(lptmX->tm_wday)
			);
		}
		
		/* Day Of Year */
		if (
			0 == StringCompare("Any", dlpszSplit[5]) ||
			0 == StringCompare("*", dlpszSplit[5])
		){ lptmX->tm_yday = -1; }
		else
		{
			StringScanFormat(
				dlpszSplit[5],
				"%d",
				&(lptmX->tm_yday)
			);
		}

		DestroySplitString(dlpszSplit);
		lpprInfo->lpParam = lptmX;

		VectorPushBack(lpceInfo->hvOnTime, &lpprInfo, 0);
	}
	else
	{
		CSTRING csBuffer[8192];
		ZeroMemory(csBuffer, sizeof(csBuffer));

		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			"%s is not a valid PublicEvent!\n",
			HeapStringValue(lpprInfo->hsEvent)
		);

		WriteFile(GetStandardError(), ANSI_RED, 0);
		WriteFile(GetStandardError(), csBuffer, 0);
		WriteFile(GetStandardError(), ANSI_RESET, 0);

		WriteLog(
			GetState()->hLog,
			csBuffer,
			LOG_FATAL,
			0
		);

		PostQuitMessage(1);
	}
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID 
New(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODPARAM lpppPod;
	LPCLOCK_EVENT lpceInfo;

	lpppPod = (LPPODPARAM)lpParam;
	lpceInfo = GlobalAllocAndZero(sizeof(CLOCK_EVENT));

	lpceInfo->hvOnReboot = CreateVector(16, sizeof(LPPODREGISTER), 0);
	lpceInfo->hvOnTime = CreateVector(16, sizeof(LPPODREGISTER), 0);
	lpceInfo->hvMinutely = CreateVector(16, sizeof(LPPODREGISTER), 0);
	lpceInfo->hvHourly = CreateVector(16, sizeof(LPPODREGISTER), 0);
	lpceInfo->hvDaily = CreateVector(16, sizeof(LPPODREGISTER), 0);
	lpceInfo->hvWeekly = CreateVector(16, sizeof(LPPODREGISTER), 0);
	lpceInfo->hvMonthly = CreateVector(16, sizeof(LPPODREGISTER), 0);
	lpceInfo->hvYearly = CreateVector(16, sizeof(LPPODREGISTER), 0);

	*(lpppPod->dlpUserData) = (LPVOID)lpceInfo;

	lpceInfo->hThread = CreateThread(__ClockProc, lpppPod, NULLPTR);
	return NULLPTR;
}


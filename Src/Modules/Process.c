/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>




PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded ProcessEvent Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
Execute(
	_In_Opt_ 	LPVOID 		lpParam
){
	HANDLE hProcess;
	LPPODPARAM lpppInfo;
	DLPSTR dlpszSplit;
	UARCHLONG ualCount, ualIndex;
	CSTRING csProgram[8192];
	CSTRING csArgs[8192];

	ZeroMemory(csProgram, sizeof(csProgram));
	ZeroMemory(csArgs, sizeof(csArgs));

	lpppInfo = (LPPODPARAM)lpParam;

	ualCount = StringSplit(
		HeapStringValue(lpppInfo->hsParam),
		",",
		&dlpszSplit
	);

	if (0 == ualCount)
	{
		DestroySplitString(dlpszSplit);
		DestroyObject(GetState()->hLog);
		PostQuitMessage(20);
	}

	StringCopySafe(
		csProgram,
		dlpszSplit[0],
		sizeof(csProgram) - 1
	);

	if (2 < ualCount)
	{
		for (
			ualIndex = 1;
			ualIndex < ualCount;
			ualIndex++
		){
			if (1 != ualIndex)
			{ StringConcatenateSafe(csArgs, " ", sizeof(csArgs) - 1); }
			StringConcatenateSafe(csArgs, "\"", sizeof(csArgs));
			StringConcatenateSafe(csArgs, dlpszSplit[ualIndex], sizeof(csArgs));
			StringConcatenateSafe(csArgs, "\"", sizeof(csArgs));
		}
	}
	
	DestroySplitString(dlpszSplit);

	PrintFormat("%s : %s\n", csProgram, csArgs);

	hProcess = CreateProcess(
		csProgram,
		csArgs,
		FALSE
	);

	WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);
	DestroyObject(hProcess);
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
ExecuteWritable(
	_In_Opt_ 	LPVOID 		lpParam
){
	HANDLE hProcess;
	LPPODPARAM lpppInfo;
	DLPSTR dlpszSplit;
	UARCHLONG ualCount;
	CSTRING csProgram[8192];
	CSTRING csArgs[8192];

	ZeroMemory(csProgram, sizeof(csProgram));
	ZeroMemory(csArgs, sizeof(csArgs));

	lpppInfo = (LPPODPARAM)lpParam;

	ualCount = StringSplit(
		HeapStringValue(lpppInfo->hsParam),
		",",
		&dlpszSplit
	);

	if (0 == ualCount)
	{
		DestroySplitString(dlpszSplit);
		DestroyObject(GetState()->hLog);
		PostQuitMessage(20);
	}

	StringCopySafe(
		csProgram,
		dlpszSplit[0],
		sizeof(csProgram) - 1
	);

	if (2 >= ualCount)
	{
		StringCopySafe(
			csArgs,
			dlpszSplit[1],
			sizeof(csProgram) - 1
		);
	}
	
	DestroySplitString(dlpszSplit);

	hProcess = CreateProcess(
		csProgram,
		csArgs,
		TRUE
	);

	WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);
	DestroyObject(hProcess);
	return NULLPTR;
}
/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <PodNet/CDevices/CGpio.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>




PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded Gpio Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
Write(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	HANDLE hGpio;
	BOOL bState;
	BYTE byPin;
	
	lpppInfo = (LPPODPARAM)lpParam;

	StringScanFormat(
		HeapStringValue(lpppInfo->hsParam),
		"%hhu,%hhu",
		&byPin,
		&bState
	);

	hGpio = OpenGpioPinAndSpecifyMode(
		(ULONG)byPin,
		GPIO_MODE_OUTPUT,
		GPIO_PULL_MODE_OFF
	);

	WriteGpio(
		hGpio,
		bState,
		NULLPTR,
		GPIO_INTERLOCKED_OPERATION
	);

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
WriteAnalog(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	HANDLE hGpio;
	BYTE byPin;
	ULONG ulValue;
	
	lpppInfo = (LPPODPARAM)lpParam;

	StringScanFormat(
		HeapStringValue(lpppInfo->hsParam),
		"%hhu,%lu",
		&byPin,
		&ulValue
	);

	hGpio = OpenGpioPinAndSpecifyMode(
		(ULONG)byPin,
		GPIO_MODE_OUTPUT,
		GPIO_PULL_MODE_OFF
	);

	WriteGpioAnalog(
		hGpio,
		ulValue,
		NULLPTR,
		GPIO_INTERLOCKED_OPERATION
	);

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
WritePwm(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	HANDLE hGpio;
	BYTE byPin;
	BYTE byValue;
	
	lpppInfo = (LPPODPARAM)lpParam;

	StringScanFormat(
		HeapStringValue(lpppInfo->hsParam),
		"%hhu,%hhu",
		&byPin,
		&byValue
	);

	hGpio = OpenGpioPinAndSpecifyMode(
		(ULONG)byPin,
		GPIO_MODE_PWM,
		GPIO_PULL_MODE_OFF
	);

	WriteGpioPwm(
		hGpio,
		byValue,
		NULLPTR,
		GPIO_INTERLOCKED_OPERATION
	);

	return NULLPTR;
}
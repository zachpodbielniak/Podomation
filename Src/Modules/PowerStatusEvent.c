/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <PodNet/CClock/CClock.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>


#define POWER_STATUS_HOST		"iot.podbielniak.com"
#define POWER_STATUS_URL		"/_/PowerStatus/"


typedef struct __POWER_STATUS_EVENT
{
	HANDLE 			hThread;
	HANDLE 			hProcess;

	HVECTOR 		hvOnSuccess;
	HVECTOR 		hvOnRestoration;
	HVECTOR			hvOnExtendedRestoration;
	HVECTOR 		hvOnOutage;
	HVECTOR			hvOnExtendedOutage;

	UARCHLONG		ualWaitTime;
	UARCHLONG 		ualExtendedThreshold;
	LPTIMESPEC		lptsOutageTime;
	BOOL 			bOutage;
	BOOL 			bExtended;
} POWER_STATUS_EVENT, *LPPOWER_STATUS_EVENT;




PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded PowerStatusEvent Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
__Proc(
	_In_Opt_	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	LPPOWER_STATUS_EVENT lppseInfo;
	HANDLE hRequest;
	UARCHLONG ualIndex;

	lpppInfo = (LPPODPARAM)lpParam;
	lppseInfo = *(lpppInfo->dlpUserData);

	hRequest = CreateHttpsRequest(
		HTTP_REQUEST_GET,
		HTTP_VERSION_1_1,
		POWER_STATUS_HOST,
		443,
		POWER_STATUS_URL,
		HTTP_CONNECTION_KEEP_ALIVE,
		NULLPTR,
		HTTP_ACCEPT_PLAIN_TEXT,
		FALSE,
		TRUE
	);

	INFINITE_LOOP()
	{
		CSTRING csRequest[8192];

		ZeroMemory(csRequest, sizeof(csRequest));
		ExecuteHttpsRequest(
			hRequest,
			csRequest,
			sizeof(csRequest) - 1,
			NULLPTR
		);

		if (0 == StringCompare(csRequest, "ONLINE\n"))
		{
			if (NULLPTR != lppseInfo->lptsOutageTime)
			{ FreeMemory(lppseInfo->lptsOutageTime); }

			/* OnRestoration */
			if (FALSE != lppseInfo->bOutage)
			{
				for (
					ualIndex = 0;
					ualIndex < VectorSize(lppseInfo->hvOnRestoration);
					ualIndex++
				){
					LPPODREGISTER lpX;
					lpX = *(LPPODREGISTER*)VectorAt(lppseInfo->hvOnRestoration, ualIndex);

					if (NULLPTR != lpX)
					{
						SignalEvent(lpX->hEvent);
						ResetEvent(lpX->hEvent);
					}
				}
				
				/* hvOnExtendedRestoration */
				if (FALSE != lppseInfo->bExtended)
				{
					lppseInfo->bExtended = FALSE;
					for (
						ualIndex = 0;
						ualIndex < VectorSize(lppseInfo->hvOnExtendedRestoration);
						ualIndex++
					){
						LPPODREGISTER lpX;
						lpX = *(LPPODREGISTER *)VectorAt(lppseInfo->hvOnExtendedRestoration, ualIndex);

						if (NULLPTR != lpX)
						{
							SignalEvent(lpX->hEvent);
							ResetEvent(lpX->hEvent);
						}
					}
				}
			}

			/* OnSuccess */
			for (
				ualIndex = 0;
				ualIndex < VectorSize(lppseInfo->hvOnSuccess);
				ualIndex++
			){
				LPPODREGISTER lpX;
				lpX = *(LPPODREGISTER*)VectorAt(lppseInfo->hvOnSuccess, ualIndex);

				if (NULLPTR != lpX)
				{
					SignalEvent(lpX->hEvent);
					ResetEvent(lpX->hEvent);
				}
			}
		}
		else
		{
			/* OnOutage */
			if (NULLPTR == lppseInfo->lptsOutageTime)
			{
				lppseInfo->lptsOutageTime = LocalAllocAndZero(sizeof(TIMESPEC));
				HighResolutionClock(lppseInfo->lptsOutageTime);
			}

			if (FALSE == lppseInfo->bOutage)
			{
				for (
					ualIndex = 0;
					ualIndex < VectorSize(lppseInfo->hvOnOutage);
					ualIndex++
				){
					LPPODREGISTER lpX;
					lpX = *(LPPODREGISTER*)VectorAt(lppseInfo->hvOnOutage, ualIndex);

					if (NULLPTR != lpX)
					{
						SignalEvent(lpX->hEvent);
						ResetEvent(lpX->hEvent);
					}
				}
			}

			lppseInfo->bOutage = TRUE;

			/* OnExtendedOutage -- Runs Once */
			if (
				FALSE == lppseInfo->bExtended && 
				HighResolutionClockAndGetMilliSeconds(lppseInfo->lptsOutageTime) >= lppseInfo->ualExtendedThreshold
			){
				lppseInfo->bExtended = TRUE; 

				for (
					ualIndex = 0;
					ualIndex < VectorSize(lppseInfo->hvOnExtendedOutage);
					ualIndex++
				){
					LPPODREGISTER lpX;
					lpX = *(LPPODREGISTER *)VectorAt(lppseInfo->hvOnExtendedOutage, ualIndex);

					if (NULLPTR != lpX)
					{
						SignalEvent(lpX->hEvent);
						ResetEvent(lpX->hEvent);
					}
				}
			}
		}
		

		Sleep(lppseInfo->ualWaitTime);
	}
}



PODOMATION_MODULE
LPVOID
RegisterEvent(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODREGISTER lpprInfo;
	LPPOWER_STATUS_EVENT lppseInfo;

	lpprInfo = (LPPODREGISTER)lpParam;
	lppseInfo = *(lpprInfo->dlpUserData);

	/* Parse Type */
	if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnSuccess"
	)){ VectorPushBack(lppseInfo->hvOnSuccess, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnRestoration"
	)){ VectorPushBack(lppseInfo->hvOnRestoration, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnExtendedRestoration"
	)){ VectorPushBack(lppseInfo->hvOnExtendedRestoration, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnOutage"
	)){ VectorPushBack(lppseInfo->hvOnOutage, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnExtendedOutage"
	)){ VectorPushBack(lppseInfo->hvOnExtendedOutage, &lpprInfo, 0); }
	else
	{
		CSTRING csBuffer[8192];
		ZeroMemory(csBuffer, sizeof(csBuffer));

		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			"%s is not a valid PowerStatus Event!\n",
			HeapStringValue(lpprInfo->hsEvent)
		);

		WriteFile(GetStandardError(), ANSI_RED, 0);
		WriteFile(GetStandardError(), csBuffer, 0);
		WriteFile(GetStandardError(), ANSI_RESET, 0);

		WriteLog(
			GetState()->hLog,
			csBuffer,
			LOG_FATAL,
			0
		);

		PostQuitMessage(1);
	}
	return NULLPTR;
}



PODOMATION_MODULE
LPVOID 
New(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODPARAM lpppPod;
	LPPOWER_STATUS_EVENT lppseInfo;

	lpppPod = (LPPODPARAM)lpParam;
	lppseInfo = GlobalAllocAndZero(sizeof(POWER_STATUS_EVENT));

	StringScanFormat(
		HeapStringValue(lpppPod->hsParam),
		"%lu,%lu",
		&(lppseInfo->ualWaitTime),
		&(lppseInfo->ualExtendedThreshold)
	);

	lppseInfo->hvOnSuccess = CreateVector(16, sizeof(LPPODREGISTER), 0);
	lppseInfo->hvOnRestoration = CreateVector(16, sizeof(LPPODREGISTER), 0);
	lppseInfo->hvOnExtendedRestoration = CreateVector(16, sizeof(LPPODREGISTER), 0);
	lppseInfo->hvOnOutage = CreateVector(16, sizeof(LPPODREGISTER), 0);
	lppseInfo->hvOnExtendedOutage = CreateVector(16, sizeof(LPPODREGISTER), 0);

	*(lpppPod->dlpUserData) = (LPVOID)lppseInfo;

	lppseInfo->hThread = CreateThread(__Proc, lpppPod, NULLPTR);
	return NULLPTR;
}

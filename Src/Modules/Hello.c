/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>



PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	PrintFormat("Loaded HelloModule\n");
	return NULLPTR;
}




INTERNAL_OPERATION
LPVOID
__OnReceive(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODREGISTER lpprInfo;
	lpprInfo = (LPPODREGISTER)lpParam;

	INFINITE_LOOP()
	{
		Sleep(5000);
		SignalEvent(lpprInfo->hEvent);
		ResetEvent(lpprInfo->hEvent);
	}
	
}




PODOMATION_MODULE
LPVOID
RegisterEvent(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	LPPODREGISTER lpprInfo;
	lpprInfo = (LPPODREGISTER)lpParam;
	
	PrintFormat(
		"POD: %p\nEvent: %p\nEventName: %s\n",
		lpprInfo->hPod,
		lpprInfo->hEvent,
		HeapStringValue(lpprInfo->hsEvent)
	);

	if (0 == StringCompare(HeapStringValue(lpprInfo->hsEvent), "OnReceive"))
	{ CreateThread(__OnReceive, lpprInfo, NULLPTR); }


	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
World(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODPARAM lpppPod;
	lpppPod = (LPPODPARAM)lpParam;

	PrintFormat(
		"From World(), called with params: %s\n",
		HeapStringValue(lpppPod->hsParam)
	);

	return NULLPTR;
}


PODOMATION_MODULE
LPVOID
Call(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	PrintFormat("Called function 'Call()'\n");
	return NULLPTR;
}
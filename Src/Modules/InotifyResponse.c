/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <PodNet/CProcess/CProcess.h>
#include <PodMQ/QueueClient/QueueClient.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>
#include "InotifyEventData.h"




PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded InotifyResponse Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
Execute(
	_In_Opt_ 	LPVOID 		lpParam
){
	HANDLE hProcess;
	LPPODPARAM lpppInfo;
	DLPSTR dlpszSplit;
	UARCHLONG ualCount, ualIndex;
	LPINOTIFY_EVENT_DATA lpiedInfo;
	CSTRING csProgram[8192];
	CSTRING csArgs[8192];

	ZeroMemory(csProgram, sizeof(csProgram));
	ZeroMemory(csArgs, sizeof(csArgs));

	lpppInfo = (LPPODPARAM)lpParam;
	lpiedInfo = *(lpppInfo->dlpCalleeData);

	ualCount = StringSplit(
		HeapStringValue(lpppInfo->hsParam),
		",",
		&dlpszSplit
	);

	if (0 == ualCount)
	{
		DestroySplitString(dlpszSplit);
		DestroyObject(GetState()->hLog);
		PostQuitMessage(3);
	}

	StringCopySafe(
		csProgram,
		dlpszSplit[0],
		sizeof(csProgram) - 1
	);

	StringPrintFormatSafe(
		csArgs,
		sizeof(csArgs) - 1,
		"--event-type %llu --file \"%s\"",
		lpiedInfo->ualEventType,
		lpiedInfo->lpieEvent->name
	);

	if (2 < ualCount)
	{
		for (
			ualIndex = 1;
			ualIndex < ualCount;
			ualIndex++
		){
			StringConcatenateSafe(csArgs, " ", sizeof(csArgs) - 1);
			StringConcatenateSafe(csArgs, "\"", sizeof(csArgs));
			StringConcatenateSafe(csArgs, dlpszSplit[ualIndex], sizeof(csArgs));
			StringConcatenateSafe(csArgs, "\"", sizeof(csArgs));
		}
	}
	
	hProcess = CreateProcess(
		csProgram,
		csArgs,
		FALSE
	);

	WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);
	DestroySplitString(dlpszSplit);
	DestroyObject(hProcess);
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
ExecuteWritable(
	_In_Opt_ 	LPVOID 		lpParam
){
	HANDLE hProcess;
	LPPODPARAM lpppInfo;
	DLPSTR dlpszSplit;
	UARCHLONG ualCount, ualIndex;
	LPINOTIFY_EVENT_DATA lpiedInfo;
	CSTRING csProgram[8192];
	CSTRING csArgs[8192];

	ZeroMemory(csProgram, sizeof(csProgram));
	ZeroMemory(csArgs, sizeof(csArgs));

	lpppInfo = (LPPODPARAM)lpParam;
	lpiedInfo = *(lpppInfo->dlpCalleeData);

	ualCount = StringSplit(
		HeapStringValue(lpppInfo->hsParam),
		",",
		&dlpszSplit
	);

	if (0 == ualCount)
	{
		DestroySplitString(dlpszSplit);
		DestroyObject(GetState()->hLog);
		PostQuitMessage(3);
	}

	StringCopySafe(
		csProgram,
		dlpszSplit[0],
		sizeof(csProgram) - 1
	);

	StringPrintFormatSafe(
		csArgs,
		sizeof(csArgs) - 1,
		"--event-type %llu --file \"%s\"",
		lpiedInfo->ualEventType,
		lpiedInfo->lpieEvent->name
	);

	if (2 < ualCount)
	{
		for (
			ualIndex = 1;
			ualIndex < ualCount;
			ualIndex++
		){
			StringConcatenateSafe(csArgs, " ", sizeof(csArgs) - 1);
			StringConcatenateSafe(csArgs, "\"", sizeof(csArgs));
			StringConcatenateSafe(csArgs, dlpszSplit[ualIndex], sizeof(csArgs));
			StringConcatenateSafe(csArgs, "\"", sizeof(csArgs));
		}
	}
	
	hProcess = CreateProcess(
		csProgram,
		csArgs,
		TRUE
	);

	WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);
	DestroySplitString(dlpszSplit);
	DestroyObject(hProcess);
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
PodMQPushFile(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	HANDLE hMq;
	USHORT usPort;
	DLPSTR dlpszSplit;
	UARCHLONG ualCount;
	QUEUE_REQUEST qrData;
	LPINOTIFY_EVENT_DATA lpiedInfo;

	lpppInfo = (LPPODPARAM)lpParam;
	lpiedInfo = *(lpppInfo->dlpCalleeData);

	ualCount = StringSplit(
		HeapStringValue(lpppInfo->hsParam),
		",",
		&dlpszSplit
	);

	if (3 != ualCount)
	{
		WriteFile(GetStandardError(), "InotifyResponse:PodMQPushFile was called without enough parameters!\n", 0);
		WriteFile(GetStandardError(), "InotifyResponse:PodMQPushFile(<HOST>,<PORT>,<QUEUE>);\n", 0);
		PostQuitMessage(3);
	}

	StringScanFormat(dlpszSplit[1], "%hu", &usPort);

	hMq = CreateMessageQueueConnection(
		dlpszSplit[0],
		usPort,
		FALSE
	);

	if (NULL_OBJECT == hMq)
	{
		WriteFile(GetStandardError(), "InotifyResponse:PodMQPushFile could not connect to PodMQ Server.\n", 0);
		PostQuitMessage(3);
	}
	
	StringCopySafe(
		qrData.csQueue,
		dlpszSplit[2],
		sizeof(qrData.csQueue) - 1
	);

	qrData.qdcData.usDataType = QUEUE_DATA_TYPE_STRING;
	qrData.qdcData.ullDataSize = StringLength(lpiedInfo->lpieEvent->name);
	qrData.qdcData.lpData = LocalAllocAndZero(qrData.qdcData.ullDataSize + 1);
	CopyMemory(
		qrData.qdcData.lpData, 
		lpiedInfo->lpieEvent->name, 
		qrData.qdcData.ullDataSize
	);

	SendMessageQueueConnectionRequest(
		hMq,
		PUSH_QUEUE,
		&qrData
	);

	if (NULLPTR != qrData.qdcData.lpData)
	{ FreeMemory(qrData.qdcData.lpData); }

	DestroyObject(hMq);
	DestroySplitString(dlpszSplit);
	return NULLPTR;
}

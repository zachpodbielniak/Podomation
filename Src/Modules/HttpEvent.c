/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <CRock/CRock.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>
#include "HttpEventData.h"




PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 			hModule,
	_In_Opt_ 	LPVOID 			lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded HttpEvent Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
__HttpProc(
	_In_Opt_	LPVOID 			lpParam
){
	LPPODPARAM lpppInfo;
	LPHTTP_EVENT lpheInfo;

	lpppInfo = (LPPODPARAM)lpParam;
	lpheInfo = *(lpppInfo->dlpUserData);


	INFINITE_LOOP()
	{ LongSleep(INFINITE_WAIT_TIME); }
}




_Call_Back_
INTERNAL_OPERATION
HTTP_STATUS_CODE
__HttpRequestHandler(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lprdData);

	LPPODPARAM lpprInfo;
	HTTP_EVENT_DATA hedData;

	lpprInfo = lpUserData;
	hedData.hscResponse = HTTP_STATUS_204_NO_CONTENT;
	hedData.lpheInfo = *(lpprInfo->dlpUserData);
	hedData.lprdData = lprdData;
	*(lpprInfo->dlpCalleeData) = &hedData;	

	SignalEvent(lpprInfo->hEvent);
	ResetEvent(lpprInfo->hEvent);

	return hedData.hscResponse;
}





PODOMATION_MODULE
LPVOID
RegisterEvent(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODREGISTER lpprInfo;
	LPHTTP_EVENT lpheInfo;
	DLPSTR dlpszSplit;
	UARCHLONG ualCount;

	lpprInfo = (LPPODREGISTER)lpParam;
	lpheInfo = *(lpprInfo->dlpUserData);

	/* Parse Type */
	if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnCreate"
	)){ VectorPushBack(lpheInfo->hvOnCreate, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnGet"
	)){ 
		DefineHttpUriProc(
			lpheInfo->hWebServer,
			"/",
			HTTP_METHOD_GET,
			__HttpRequestHandler,
			lpprInfo
		);
	}
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnPut"
	)){ 
		DefineHttpUriProc(
			lpheInfo->hWebServer,
			"/",
			HTTP_METHOD_PUT,
			__HttpRequestHandler,
			lpprInfo
		);
	}
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnPost"
	)){ 
		DefineHttpUriProc(
			lpheInfo->hWebServer,
			"/",
			HTTP_METHOD_POST,
			__HttpRequestHandler,
			lpprInfo
		);
	}
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnDelete"
	)){ 
		DefineHttpUriProc(
			lpheInfo->hWebServer,
			"/",
			HTTP_METHOD_DELETE,
			__HttpRequestHandler,
			lpprInfo
		);
	}
	else
	{
		ualCount = StringSplit(
			HeapStringValue(lpprInfo->hsEvent),
			"-",
			&dlpszSplit
		);

		/* Format Expect: OnGet-/MyEndpoint */
		if (2 != ualCount)
		{
			WriteFile(GetStandardError(), "Format Expected: OnGet-/MyEndpoint\n", 0);
			PostQuitMessage(3);
		}

		if ('/' != dlpszSplit[1][0])
		{
			WriteFile(GetStandardError(), "Format Expected: OnGet-/MyEndpoint\n", 0);
			WriteFile(GetStandardError(), "Notice The '/' Must Be Present!\n", 0);
			PostQuitMessage(3);
		}

		if (0 == StringCompare("OnGet", dlpszSplit[0]))
		{
			DefineHttpUriProc(
				lpheInfo->hWebServer,
				dlpszSplit[1],
				HTTP_METHOD_GET,
				__HttpRequestHandler,
				lpprInfo
			);
		}
		else if (0 == StringCompare("OnPut", dlpszSplit[0]))
		{
			DefineHttpUriProc(
				lpheInfo->hWebServer,
				dlpszSplit[1],
				HTTP_METHOD_PUT,
				__HttpRequestHandler,
				lpprInfo
			);
		}
		else if (0 == StringCompare("OnPost", dlpszSplit[0]))
		{
			DefineHttpUriProc(
				lpheInfo->hWebServer,
				dlpszSplit[1],
				HTTP_METHOD_POST,
				__HttpRequestHandler,
				lpprInfo
			);
		}
		else if (0 == StringCompare("OnDelete", dlpszSplit[0]))
		{
			DefineHttpUriProc(
				lpheInfo->hWebServer,
				dlpszSplit[1],
				HTTP_METHOD_DELETE,
				__HttpRequestHandler,
				lpprInfo
			);
		}
	}
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID 
New(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODPARAM lpppPod;
	LPHTTP_EVENT lpheInfo;
	DLPSTR dlpszSplit;
	UARCHLONG ualCount;

	lpppPod = (LPPODPARAM)lpParam;
	lpheInfo = GlobalAllocAndZero(sizeof(HTTP_EVENT));

	ualCount = StringSplit(
		HeapStringValue(lpppPod->hsParam),
		",",
		&dlpszSplit
	);

	if (2 != ualCount)
	{
		WriteFile(GetStandardOut(), ANSI_RED "Expected two arguments to PublicIpEvent:New(<IP>,<WAIT_TIME>)\n" ANSI_RESET, 0);
		DestroyObject(GetState()->hLog);
		PostQuitMessage(2);
	}

	lpheInfo->dlpszBindAddresses = GlobalAllocAndZero(sizeof(LPSTR));
	lpheInfo->dlpszBindAddresses[0] = GlobalAllocAndZero(StringLength(dlpszSplit[0]) + 1);
	CopyMemory(lpheInfo->dlpszBindAddresses[0], dlpszSplit[0], StringLength(dlpszSplit[0]));

	lpheInfo->lpusPorts = GlobalAllocAndZero(sizeof(USHORT));
	StringScanFormat(
		dlpszSplit[1],
		"%hu",
		&(lpheInfo->lpusPorts[0])
	);

	lpheInfo->hWebServer = CreateHttpServer(
		"Podomation",
		(DLPCSTR)lpheInfo->dlpszBindAddresses,
		1,
		lpheInfo->lpusPorts,
		1,
		"podomation",
		16384
	);

	*(lpppPod->dlpUserData) = (LPVOID)lpheInfo;

	lpheInfo->hThread = CreateThread(__HttpProc, lpppPod, NULLPTR);
	return NULLPTR;
}

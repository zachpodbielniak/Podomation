/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <PodNet/CNetworking/CSmtp.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>




PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded Print Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}



PODOMATION_MODULE
LPVOID
Send(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	lpppInfo = (LPPODPARAM)lpParam;

	HANDLE hSmtp;
	CSTRING csFrom[512];
	CSTRING csTo[512];
	CSTRING csSubject[512];
	CSTRING csBody[512];
	DLPSTR dlpszSplit;
	UARCHLONG ualCount;

	ZeroMemory(csFrom, sizeof(csFrom));
	ZeroMemory(csTo, sizeof(csTo));
	ZeroMemory(csSubject, sizeof(csSubject));
	ZeroMemory(csBody, sizeof(csBody));

	ualCount = StringSplit(
		HeapStringValue(lpppInfo->hsParam),
		",",
		&dlpszSplit
	);

	if (4 != ualCount)
	{ 
		DestroySplitString(dlpszSplit);
		WriteFile(GetStandardOut(), ANSI_RED "Send() was not called with 4 parameters!\n" ANSI_RESET, 0);

		DestroyObject(GetState()->hLog);
		PostQuitMessage(2);
	}

	StringCopySafe(csFrom, dlpszSplit[0], sizeof(csFrom) - 1);
	StringCopySafe(csTo, dlpszSplit[1], sizeof(csTo) - 1);
	StringCopySafe(csSubject, dlpszSplit[2], sizeof(csSubject) - 1);
	StringCopySafe(csBody, dlpszSplit[3], sizeof(csBody) - 1);

	hSmtp = CreateSmtpConnection(
		SMTP_PROTOCOL_SSL,
		"disroot.org",
		NULLPTR,
		NULLPTR,
		TRUE,
		FALSE
	);

	SendEmail(
		hSmtp,
		csFrom,
		csTo,
		NULLPTR,
		csSubject,
		csBody
	);

	DestroySplitString(dlpszSplit);
	DestroyObject(hSmtp);
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
SendProcessOutput(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	lpppInfo = (LPPODPARAM)lpParam;

	HANDLE hSmtp;
	HANDLE hProcess;
	HANDLE hOutput;
	CSTRING csFrom[512];
	CSTRING csTo[512];
	CSTRING csSubject[512];
	CSTRING csBodyBuffer[8192];
	CSTRING csProgram[512];
	CSTRING csArgs[512];
	HSTRING hsBody;
	LPSTR lpszBuffer;
	DLPSTR dlpszSplit;
	UARCHLONG ualCount, ualIndex;

	ZeroMemory(csFrom, sizeof(csFrom));
	ZeroMemory(csTo, sizeof(csTo));
	ZeroMemory(csSubject, sizeof(csSubject));
	ZeroMemory(csBodyBuffer, sizeof(csBodyBuffer));
	ZeroMemory(csProgram, sizeof(csProgram));
	ZeroMemory(csArgs, sizeof(csArgs));

	ualCount = StringSplit(
		HeapStringValue(lpppInfo->hsParam),
		",",
		&dlpszSplit
	);

	if (4 > ualCount)
	{ 
		DestroySplitString(dlpszSplit);
		WriteFile(GetStandardOut(), ANSI_RED "SendProcessOutput() was called with less than 4 parameters!\n" ANSI_RESET, 0);

		DestroyObject(GetState()->hLog);
		PostQuitMessage(2);
	}

	StringCopySafe(csFrom, dlpszSplit[0], sizeof(csFrom) - 1);
	StringCopySafe(csTo, dlpszSplit[1], sizeof(csTo) - 1);
	StringCopySafe(csSubject, dlpszSplit[2], sizeof(csSubject) - 1);
	StringCopySafe(csProgram, dlpszSplit[3], sizeof(csProgram) - 1);

	if (4 < ualCount)
	{
		for (
			ualIndex = 4;
			ualIndex < ualCount;
			ualIndex++
		){ 
			if (4 != ualIndex)
			{ StringConcatenateSafe(csArgs, " ", sizeof(csArgs) - 1); }
			StringConcatenateSafe(csArgs, dlpszSplit[ualIndex], sizeof(csArgs) - 1); 
		}
	}

	hProcess = CreateProcess(
		csProgram,
		csArgs,
		FALSE
	);

	if (NULL_OBJECT == hProcess)
	{
		WriteFile(GetStandardOut(), "Unable to open program\n", 0);
		DestroyObject(GetState()->hLog);
		PostQuitMessage(2);
	}

	hOutput = GetProcessOutput(hProcess);
	lpszBuffer = (LPSTR)csBodyBuffer;
	hsBody = NULLPTR;
	WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);

	while (-1 != ReadLineFromFile(hOutput, &lpszBuffer, sizeof(csBodyBuffer) - 1, 0))
	{
		if (NULLPTR == hsBody)
		{ hsBody = CreateHeapString(csBodyBuffer); }
		else
		{ HeapStringAppend(hsBody, csBodyBuffer); }
	}


	hSmtp = CreateSmtpConnection(
		SMTP_PROTOCOL_SSL,
		"disroot.org",
		NULLPTR,
		NULLPTR,
		TRUE,
		FALSE
	);

	SendEmail(
		hSmtp,
		csFrom,
		csTo,
		NULLPTR,
		csSubject,
		HeapStringValue(hsBody)
	);

	DestroySplitString(dlpszSplit);
	DestroyObject(hProcess);
	DestroyObject(hSmtp);
	DestroyHeapString(hsBody);
	return NULLPTR;
}
/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>




PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded Log Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
WriteInfo(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	CSTRING csBuffer[8192];
	
	lpppInfo = (LPPODPARAM)lpParam;
	ZeroMemory(csBuffer, sizeof(csBuffer));

	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 1,
		"%s\n",
		HeapStringValue(lpppInfo->hsParam)
	);

	WriteLog(
		GetState()->hLog,
		csBuffer,
		LOG_INFO,
		0
	);
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
WriteError(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	CSTRING csBuffer[8192];
	
	lpppInfo = (LPPODPARAM)lpParam;
	ZeroMemory(csBuffer, sizeof(csBuffer));

	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 1,
		"%s\n",
		HeapStringValue(lpppInfo->hsParam)
	);

	WriteLog(
		GetState()->hLog,
		csBuffer,
		LOG_ERROR,
		0
	);
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
WriteDebug(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	CSTRING csBuffer[8192];
	
	lpppInfo = (LPPODPARAM)lpParam;
	ZeroMemory(csBuffer, sizeof(csBuffer));

	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 1,
		"%s\n",
		HeapStringValue(lpppInfo->hsParam)
	);

	WriteLog(
		GetState()->hLog,
		csBuffer,
		LOG_DEBUG,
		0
	);
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
WriteWarning(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	CSTRING csBuffer[8192];
	
	lpppInfo = (LPPODPARAM)lpParam;
	ZeroMemory(csBuffer, sizeof(csBuffer));

	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 1,
		"%s\n",
		HeapStringValue(lpppInfo->hsParam)
	);

	WriteLog(
		GetState()->hLog,
		csBuffer,
		LOG_WARNING,
		0
	);
	return NULLPTR;
}
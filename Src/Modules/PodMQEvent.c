/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>
#include <PodMQ/QueueClient/QueueClient.h>


typedef struct __PODMQ_EVENT
{
	HANDLE 		hThread;
	HANDLE 		hPodMQ;
	ULONGLONG 	ullWaitTime;
	HVECTOR 	hvOnNewData;
	HVECTOR 	hvOnPostData;
	HVECTOR 	hvOnStart;
	HSTRING 	hsQueue;
	HSTRING 	hsServer;
	USHORT 		usPort;
} PODMQ_EVENT, *LPPODMQ_EVENT;




PODOMATION_MODULE
LPVOID
__PodMQProc(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	LPPODMQ_EVENT lppmqInfo;
	UARCHLONG ualIndex;
	ULONGLONG ullWaitTime;
	VOLATILE LPQUEUE_REQUEST lpqrData;

	/* Needed for some ungodly reason */
	Sleep(200);

	lpppInfo = (LPPODPARAM)lpParam;
	lppmqInfo = *(lpppInfo->dlpUserData);
	lpqrData = GlobalAllocAndZero(sizeof(QUEUE_REQUEST));
	
	StringCopySafe(lpqrData->csQueue, HeapStringValue(lppmqInfo->hsQueue), sizeof(lpqrData->csQueue));

	/* OnStart */
	for (
		ualIndex = 0;
		ualIndex < VectorSize(lppmqInfo->hvOnStart);
		ualIndex++
	){
		LPPODREGISTER lpX;
		lpX = *(LPPODREGISTER *)VectorAt(lppmqInfo->hvOnStart, ualIndex);

		if (NULLPTR != lpX)
		{ 
			SignalEvent(lpX->hEvent);
			ResetEvent(lpX->hEvent);
		}

	}

	INFINITE_LOOP()
	{
		TIMESPEC tmSpec;
		HighResolutionClock(&tmSpec);

		ZeroMemory(&(lpqrData->qdcData), sizeof(QUEUE_DATA_DESCRIPTOR));

		SendMessageQueueConnectionRequest(
			lppmqInfo->hPodMQ,
			POP_QUEUE,
			lpqrData
		);

		/* Set Callee Data */
		*(lpppInfo->dlpCalleeData) = lpqrData;


		if (NULLPTR != lpqrData->qdcData.lpData)
		{
			for (
				ualIndex = 0;
				ualIndex < VectorSize(lppmqInfo->hvOnNewData);
				ualIndex++
			){
				LPPODREGISTER lpX;
				lpX = *(LPPODREGISTER *)VectorAt(lppmqInfo->hvOnNewData, ualIndex);

				if (NULLPTR != lpX)
				{
					SignalEvent(lpX->hEvent);
					ResetEvent(lpX->hEvent);
				}
			}
		}		

		ullWaitTime = lppmqInfo->ullWaitTime - HighResolutionClockAndGetMilliSeconds(&tmSpec);
		ullWaitTime = (ullWaitTime > lppmqInfo->ullWaitTime) ? 0 : ullWaitTime;
		Sleep(ullWaitTime);
	}

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
RegisterEvent(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODREGISTER lpprInfo;
	LPPODMQ_EVENT lppmqInfo;

	lpprInfo = (LPPODREGISTER)lpParam;
	lppmqInfo = *(lpprInfo->dlpUserData);
	
	/* Parse Type */
	if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnStart"
	)){ VectorPushBack(lppmqInfo->hvOnStart, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnNewData"
	)){ VectorPushBack(lppmqInfo->hvOnNewData, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnPostData"
	)){ VectorPushBack(lppmqInfo->hvOnPostData, &lpprInfo, 0); }
	else
	{
		CSTRING csBuffer[8192];
		ZeroMemory(csBuffer, sizeof(csBuffer));

		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			"%s is not a valid PodMQ Event!\n",
			HeapStringValue(lpprInfo->hsEvent)
		);

		WriteFile(GetStandardError(), ANSI_RED, 0);
		WriteFile(GetStandardError(), csBuffer, 0);
		WriteFile(GetStandardError(), ANSI_RESET, 0);

		WriteLog(
			GetState()->hLog,
			csBuffer,
			LOG_FATAL,
			0
		);

		PostQuitMessage(1);
	}
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID 
New(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODPARAM lpppPod;
	LPPODMQ_EVENT lppmqInfo;
	DLPSTR dlpszSplit;
	UARCHLONG ualCount;
	CSTRING csAddress[0x32];
	CSTRING csQueue[0x40];

	ZeroMemory(csAddress, sizeof(csAddress));
	ZeroMemory(csQueue, sizeof(csQueue));

	lpppPod = (LPPODPARAM)lpParam;
	lppmqInfo = GlobalAllocAndZero(sizeof(PODMQ_EVENT));

	lppmqInfo->hvOnNewData = CreateVector(16, sizeof(LPPODREGISTER), 0);
	lppmqInfo->hvOnPostData = CreateVector(16, sizeof(LPPODREGISTER), 0);
	lppmqInfo->hvOnStart = CreateVector(16, sizeof(LPPODREGISTER), 0);
	
	/* Args */
	ualCount = StringSplit(
		HeapStringValue(lpppPod->hsParam),
		",",
		&dlpszSplit
	);

	if (4 != ualCount)
	{
		WriteFile(GetStandardError(), ANSI_RED, 0);
		WriteFile(GetStandardError(), "PodMQEvent:New(<HOST>,<PORT>,<WAIT_TIME>,<QUEUE>);\n", 0);
		WriteFile(GetStandardError(), ANSI_RESET, 0);

		PostQuitMessage(1);
	}

	StringScanFormat(dlpszSplit[1], "%hu", &(lppmqInfo->usPort));
	StringScanFormat(dlpszSplit[2], "%lu", &(lppmqInfo->ullWaitTime));
	lppmqInfo->hsQueue = CreateHeapString(dlpszSplit[3]);
	lppmqInfo->hsServer = CreateHeapString(dlpszSplit[0]);

	lppmqInfo->hPodMQ = CreateMessageQueueConnection(
		HeapStringValue(lppmqInfo->hsServer),
		lppmqInfo->usPort,
		FALSE
	);

	*(lpppPod->dlpUserData) = (LPVOID)lppmqInfo;
	lppmqInfo->hThread = CreateThread(__PodMQProc, lpppPod, NULLPTR);
	return NULLPTR;
}
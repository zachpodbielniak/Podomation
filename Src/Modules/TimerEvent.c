/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>


typedef struct __TIMER_EVENT
{
	ULONGLONG 		ullTickTime;
} TIMER_EVENT, *LPTIMER_EVENT;




PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded TimerEvent Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
__OnTick(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODREGISTER lpprInfo;
	LPTIMER_EVENT lpteInfo;
	ULONGLONG ullTick;

	lpprInfo = (LPPODREGISTER)lpParam;
	lpteInfo = *(lpprInfo->dlpUserData);
	ullTick = lpteInfo->ullTickTime;

	Sleep(ullTick);
	INFINITE_LOOP()
	{
		SignalEvent(lpprInfo->hEvent);
		ResetEvent(lpprInfo->hEvent);
		Sleep(ullTick);
	}

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
RegisterEvent(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODREGISTER lpprInfo;
	lpprInfo = (LPPODREGISTER)lpParam;
	
	/* Parse Type */
	if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnTick"
	)){ CreateThread(__OnTick, lpprInfo, 0); }
	else
	{
		CSTRING csBuffer[8192];
		ZeroMemory(csBuffer, sizeof(csBuffer));

		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			"%s is not a valid TimerEvent!\n",
			HeapStringValue(lpprInfo->hsEvent)
		);

		WriteFile(GetStandardError(), ANSI_RED, 0);
		WriteFile(GetStandardError(), csBuffer, 0);
		WriteFile(GetStandardError(), ANSI_RESET, 0);

		WriteLog(
			GetState()->hLog,
			csBuffer,
			LOG_FATAL,
			0
		);

		PostQuitMessage(1);
	}
	
	return NULLPTR;
}



PODOMATION_MODULE
LPVOID 
New(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODPARAM lpppPod;
	LPTIMER_EVENT lpteInfo;

	lpppPod = (LPPODPARAM)lpParam;
	lpteInfo = GlobalAllocAndZero(sizeof(TIMER_EVENT));

	StringScanFormat(
		HeapStringValue(lpppPod->hsParam),
		"%lu",
		&(lpteInfo->ullTickTime)
	);

	*(lpppPod->dlpUserData) = (LPVOID)lpteInfo;
	return NULLPTR;
}
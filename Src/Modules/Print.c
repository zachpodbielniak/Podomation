/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>




PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded Print Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
String(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	lpppInfo = (LPPODPARAM)lpParam;
	PrintFormat("%s", HeapStringValue(lpppInfo->hsParam));
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
StringLine(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	lpppInfo = (LPPODPARAM)lpParam;
	PrintFormat("%s\n", HeapStringValue(lpppInfo->hsParam));
	return NULLPTR;
}



PODOMATION_MODULE
LPVOID
StringRed(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	lpppInfo = (LPPODPARAM)lpParam;
	PrintFormat(ANSI_RED "%s" ANSI_RESET, HeapStringValue(lpppInfo->hsParam));
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
StringRedLine(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	lpppInfo = (LPPODPARAM)lpParam;
	PrintFormat(ANSI_RED "%s\n" ANSI_RESET, HeapStringValue(lpppInfo->hsParam));
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
StringBlue(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	lpppInfo = (LPPODPARAM)lpParam;
	PrintFormat(ANSI_BLUE "%s" ANSI_RESET, HeapStringValue(lpppInfo->hsParam));
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
StringBlueLine(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	lpppInfo = (LPPODPARAM)lpParam;
	PrintFormat(ANSI_BLUE "%s\n" ANSI_RESET, HeapStringValue(lpppInfo->hsParam));
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
StringGreen(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	lpppInfo = (LPPODPARAM)lpParam;
	PrintFormat(ANSI_GREEN "%s" ANSI_RESET, HeapStringValue(lpppInfo->hsParam));
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
StringGreenLine(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	lpppInfo = (LPPODPARAM)lpParam;
	PrintFormat(ANSI_GREEN "%s\n" ANSI_RESET, HeapStringValue(lpppInfo->hsParam));
	return NULLPTR;
}
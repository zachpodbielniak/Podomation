/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>
#include <PodMQ/QueueClient/QueueClient.h>




PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded PodMQ Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
PrintDataString(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	lpppInfo = (LPPODPARAM)lpParam;
	
	if (NULLPTR != lpppInfo->dlpCalleeData)
	{
		LPQUEUE_REQUEST lpqrData;
		lpqrData = *(lpppInfo->dlpCalleeData);

		if (NULLPTR != lpqrData->qdcData.lpData)
		{ PrintFormat("%s", lpqrData->qdcData.lpData); }
	}

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
PrintDataStringNewLine(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	lpppInfo = (LPPODPARAM)lpParam;
	
	if (NULLPTR != lpppInfo->dlpCalleeData)
	{
		LPQUEUE_REQUEST lpqrData;
		lpqrData = *(lpppInfo->dlpCalleeData);

		if (NULLPTR != lpqrData->qdcData.lpData)
		{ PrintFormat("%s\n", lpqrData->qdcData.lpData); }
	}

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
PrintQueueName(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	lpppInfo = (LPPODPARAM)lpParam;
	
	if (NULLPTR != lpppInfo->dlpCalleeData)
	{
		LPQUEUE_REQUEST lpqrData;
		lpqrData = *(lpppInfo->dlpCalleeData);

		PrintFormat("%s", lpqrData->csQueue); 
	}

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
PrintQueueNameNewLine(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	lpppInfo = (LPPODPARAM)lpParam;
	
	if (NULLPTR != lpppInfo->dlpCalleeData)
	{
		LPQUEUE_REQUEST lpqrData;
		lpqrData = *(lpppInfo->dlpCalleeData);

		PrintFormat("%s\n", lpqrData->csQueue); 
	}

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
Execute(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	HANDLE hProcess;
	CSTRING csArgs[4096];
	LPQUEUE_REQUEST lpqrData;

	ZeroMemory(csArgs, sizeof(csArgs));
	lpppInfo = (LPPODPARAM)lpParam;
	lpqrData = *(lpppInfo->dlpCalleeData);

	StringPrintFormatSafe(
		csArgs,
		sizeof(csArgs) - 1,
		"--queue \"%s\" --data-size %lu --data-type %hu --data \"%s\"",
		lpqrData->csQueue,
		lpqrData->qdcData.ullDataSize,
		lpqrData->qdcData.usDataType,
		(LPSTR)lpqrData->qdcData.lpData
	);

	hProcess = CreateProcess(
		HeapStringValue(lpppInfo->hsParam),
		(LPCSTR)csArgs,
		FALSE
	);

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
ExecuteWritable(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	HANDLE hProcess;
	CSTRING csArgs[4096];
	LPQUEUE_REQUEST lpqrData;

	ZeroMemory(csArgs, sizeof(csArgs));
	lpppInfo = (LPPODPARAM)lpParam;
	lpqrData = *(lpppInfo->dlpCalleeData);

	StringPrintFormatSafe(
		csArgs,
		sizeof(csArgs) - 1,
		"--queue \"%s\" --data-size %lu --data-type %hu --data \"%s\"",
		lpqrData->csQueue,
		lpqrData->qdcData.ullDataSize,
		lpqrData->qdcData.usDataType,
		(LPSTR)lpqrData->qdcData.lpData
	);

	hProcess = CreateProcess(
		HeapStringValue(lpppInfo->hsParam),
		(LPCSTR)csArgs,
		TRUE
	);

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
Create(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	HANDLE hMq;
	USHORT usPort;
	DLPSTR dlpszSplit;
	UARCHLONG ualCount;
	QUEUE_REQUEST qrData;

	lpppInfo = (LPPODPARAM)lpParam;
	ualCount = StringSplit(
		HeapStringValue(lpppInfo->hsParam),
		",",
		&dlpszSplit
	);

	if (3 != ualCount)
	{
		WriteFile(GetStandardError(), "PodMQ:CreateQueue was called without enough parameters!\n", 0);
		WriteFile(GetStandardError(), "PodMQ:CreateQueue(<HOST>,<PORT>,<QUEUE>);\n", 0);
		PostQuitMessage(3);
	}

	StringScanFormat(dlpszSplit[1], "%hu", &usPort);

	hMq = CreateMessageQueueConnection(
		dlpszSplit[0],
		usPort,
		FALSE
	);

	if (NULL_OBJECT == hMq)
	{
		WriteFile(GetStandardError(), "PodMQ:PushString could not connect to PodMQ Server.\n", 0);
		PostQuitMessage(3);
	}
	
	StringCopySafe(
		qrData.csQueue,
		dlpszSplit[2],
		sizeof(qrData.csQueue) - 1
	);

	qrData.qdcData.usDataType = 0;
	qrData.qdcData.ullDataSize = 0;
	qrData.qdcData.lpData = NULLPTR;

	SendMessageQueueConnectionRequest(
		hMq,
		CREATE_QUEUE,
		&qrData
	);

	if (NULLPTR != qrData.qdcData.lpData)
	{ FreeMemory(qrData.qdcData.lpData); }

	DestroyObject(hMq);
	DestroySplitString(dlpszSplit);
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
PushString(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	HANDLE hMq;
	USHORT usPort;
	DLPSTR dlpszSplit;
	UARCHLONG ualCount;
	QUEUE_REQUEST qrData;

	lpppInfo = (LPPODPARAM)lpParam;
	ualCount = StringSplit(
		HeapStringValue(lpppInfo->hsParam),
		",",
		&dlpszSplit
	);

	if (4 != ualCount)
	{
		WriteFile(GetStandardError(), "PodMQ:PushString was called without enough parameters!\n", 0);
		WriteFile(GetStandardError(), "PodMQ:PushString(<HOST>,<PORT>,<QUEUE>,<DATA>);\n", 0);
		PostQuitMessage(3);
	}

	StringScanFormat(dlpszSplit[1], "%hu", &usPort);

	hMq = CreateMessageQueueConnection(
		dlpszSplit[0],
		usPort,
		FALSE
	);

	if (NULL_OBJECT == hMq)
	{
		WriteFile(GetStandardError(), "PodMQ:PushString could not connect to PodMQ Server.\n", 0);
		PostQuitMessage(3);
	}
	
	StringCopySafe(
		qrData.csQueue,
		dlpszSplit[2],
		sizeof(qrData.csQueue) - 1
	);

	qrData.qdcData.usDataType = QUEUE_DATA_TYPE_STRING;
	qrData.qdcData.ullDataSize = StringLength(dlpszSplit[3]);
	qrData.qdcData.lpData = LocalAllocAndZero(qrData.qdcData.ullDataSize + 1);
	CopyMemory(qrData.qdcData.lpData, dlpszSplit[3], qrData.qdcData.ullDataSize);

	SendMessageQueueConnectionRequest(
		hMq,
		PUSH_QUEUE,
		&qrData
	);

	if (NULLPTR != qrData.qdcData.lpData)
	{ FreeMemory(qrData.qdcData.lpData); }

	DestroyObject(hMq);
	DestroySplitString(dlpszSplit);
	return NULLPTR;
}
/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <CRock/CRock.h>
#include <CRock/Post.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>
#include "HttpEventData.h"




PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded HttpResponse Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
ServeText(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	LPHTTP_EVENT_DATA lphedInfo;

	lpppInfo = lpParam;
	lphedInfo = *(lpppInfo->dlpCalleeData);

	RequestAddResponse(
		lphedInfo->lprdData->hRequest,
		(LPVOID)HeapStringValue(lpppInfo->hsParam),
		StringLength(HeapStringValue(lpppInfo->hsParam))
	);

	lphedInfo->hscResponse = HTTP_STATUS_200_OK;
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID 
ServeFile(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	LPHTTP_EVENT_DATA lphedInfo;
	HANDLE hFile;
	ARCHLONG alRead;
	CSTRING csBuffer[8192];
	LPSTR lpszBuffer;

	lpppInfo = lpParam;
	lphedInfo = *(lpppInfo->dlpCalleeData);

	hFile = OpenFile(
		HeapStringValue(lpppInfo->hsParam),
		FILE_PERMISSION_READ,
		0
	);

	if (NULL_OBJECT == hFile)
	{ 	
		lphedInfo->hscResponse = HTTP_STATUS_500_INTERNAL_SERVER_ERROR;
		return (LPVOID)1;
	}

	ZeroMemory(csBuffer, sizeof(csBuffer));
	while (-1 != (alRead = ReadLineFromFile(
		hFile,
		&lpszBuffer,
		sizeof(csBuffer) - 1,
		0
	))){
		RequestAddResponse(
			lphedInfo->lprdData->hRequest,
			lpszBuffer,
			alRead
		);
		ZeroMemory(csBuffer, sizeof(csBuffer));
	}

	DestroyObject(hFile);
	lphedInfo->hscResponse = HTTP_STATUS_200_OK;
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID 
ServeDirectory(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	LPHTTP_EVENT_DATA lphedInfo;
	HANDLE hFile;
	ARCHLONG alRead;
	CSTRING csFile[512];
	CSTRING csDir[1024];
	CSTRING csBuffer[8192];
	LPSTR lpszBuffer;
	UARCHLONG ualCount;
	UARCHLONG ualIndex;
	DLPSTR dlpszSplit;
	LPQUERYSTRING_DATA lpqsdFile;
	BOOL bParam;

	lpppInfo = lpParam;
	lphedInfo = *(lpppInfo->dlpCalleeData);
	bParam = FALSE;

	ZeroMemory(csFile, sizeof(csFile));
	ZeroMemory(csDir, sizeof(csDir));

	ualCount = StringSplit(
		HeapStringValue(lpppInfo->hsParam),
		",",
		&dlpszSplit
	);

	/* Default To index.html */
	if (1 == ualCount)
	{
		StringCopySafe(
			csDir,
			dlpszSplit[0],
			sizeof(csDir) - 1
		);
		StringPrintFormatSafe(
			csFile,
			sizeof(csFile) - 1,
			"%s/%s",
			csDir,
			"index.html"
		);
	}
	/* <DIR>,<DEFAULT_FILE> */
	else 
	{
		StringCopySafe(
			csDir,
			dlpszSplit[0],
			sizeof(csDir) - 1
		);
		StringPrintFormatSafe(
			csFile,
			sizeof(csFile) - 1,
			"%s/%s",
			csDir,
			dlpszSplit[1]
		);
	}

	/* Process Query String And Find File= */
	ProcessQueryStringParameters(lphedInfo->lprdData->hRequest);

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lphedInfo->lprdData->hvQueryParameters);
		ualIndex++
	){
		lpqsdFile = VectorAt(lphedInfo->lprdData->hvQueryParameters, ualIndex);
		if (NULLPTR == lpqsdFile)
		{ continue; }

		if (0 == StringCompare("File", lpqsdFile->lpszKey))
		{ 
			bParam = TRUE;
			JUMP(__GOT_FILE_PARAM); 
		}
	}
	__GOT_FILE_PARAM:

	if (TRUE == bParam)
	{
		ZeroMemory(csFile, sizeof(csFile));

		/* Safety Check */
		if (NULLPTR != StringInString(lpqsdFile->lpszValue, ".."))
		{
			lphedInfo->hscResponse = HTTP_STATUS_400_BAD_REQUEST;
			DestroySplitString(dlpszSplit);
			return (LPVOID)1;
		}

		StringPrintFormatSafe(
			csFile,
			sizeof(csFile) - 1,
			"%s/%s",
			csDir,
			lpqsdFile->lpszValue
		);
	}

	hFile = OpenFile(
		csFile,
		FILE_PERMISSION_READ,
		0
	);

	if (NULL_OBJECT == hFile)
	{ 	
		lphedInfo->hscResponse = HTTP_STATUS_500_INTERNAL_SERVER_ERROR;
		DestroySplitString(dlpszSplit);
		return (LPVOID)1;
	}

	ZeroMemory(csBuffer, sizeof(csBuffer));
	while (-1 != (alRead = ReadLineFromFile(
		hFile,
		&lpszBuffer,
		sizeof(csBuffer) - 1,
		0
	))){
		RequestAddResponse(
			lphedInfo->lprdData->hRequest,
			lpszBuffer,
			alRead
		);
		ZeroMemory(csBuffer, sizeof(csBuffer));
	}

	DestroySplitString(dlpszSplit);
	DestroyObject(hFile);
	lphedInfo->hscResponse = HTTP_STATUS_200_OK;
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
Execute(
	_In_Opt_ 	LPVOID 		lpParam
){
	HANDLE hProcess;
	LPPODPARAM lpppInfo;
	LPHTTP_EVENT_DATA lphedInfo;
	DLPSTR dlpszSplit;
	UARCHLONG ualCount, ualIndex;
	CSTRING csProgram[8192];
	CSTRING csArgs[8192];
	LPQUERYSTRING_DATA lpqsdFile;
	LPSTR lpszHttpMethod;
	BOOL bStartedArgs;

	ZeroMemory(csProgram, sizeof(csProgram));
	ZeroMemory(csArgs, sizeof(csArgs));

	lpppInfo = (LPPODPARAM)lpParam;
	lphedInfo = *(lpppInfo->dlpCalleeData);
	bStartedArgs = FALSE;

	ualCount = StringSplit(
		HeapStringValue(lpppInfo->hsParam),
		",",
		&dlpszSplit
	);

	if (0 == ualCount)
	{
		DestroySplitString(dlpszSplit);
		DestroyObject(GetState()->hLog);
		PostQuitMessage(20);
	}

	StringCopySafe(
		csProgram,
		dlpszSplit[0],
		sizeof(csProgram) - 1
	);

	HTTP_METHOD_TO_STRING(lphedInfo->lprdData->hmType, lpszHttpMethod);

	StringPrintFormatSafe(
		csArgs,
		sizeof(csArgs) - 1,
		"--http-method %s --http-uri \"%s\" --content-type \"%s\"",
		lpszHttpMethod,
		lphedInfo->lprdData->lpszRequestUri,
		lphedInfo->lprdData->lpszContentType
	);

	if (1 < ualCount)
	{
		for (
			ualIndex = 1;
			ualIndex < ualCount;
			ualIndex++
		){
			StringConcatenateSafe(csArgs, " ", sizeof(csArgs) - 1);
			StringConcatenateSafe(csArgs, "\"", sizeof(csArgs));
			StringConcatenateSafe(csArgs, dlpszSplit[ualIndex], sizeof(csArgs));
			StringConcatenateSafe(csArgs, "\"", sizeof(csArgs));
		}
	}

	ProcessQueryStringParameters(lphedInfo->lprdData->hRequest);

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lphedInfo->lprdData->hvQueryParameters);
		ualIndex++
	){
		lpqsdFile = VectorAt(lphedInfo->lprdData->hvQueryParameters, ualIndex);
		if (NULLPTR == lpqsdFile)
		{ continue; }

		StringConcatenateSafe(csArgs, " ", sizeof(csArgs) - 1);
		StringConcatenateSafe(csArgs, "\"--", sizeof(csArgs) - 1);
		StringConcatenateSafe(csArgs, lpqsdFile->lpszKey, sizeof(csArgs) - 1);
		StringConcatenateSafe(csArgs, "\" \"", sizeof(csArgs) - 1);
		StringConcatenateSafe(csArgs, lpqsdFile->lpszValue, sizeof(csArgs) - 1);
		StringConcatenateSafe(csArgs, "\"", sizeof(csArgs) - 1);
	}

	if (FALSE != PostIsMultipartForm(lphedInfo->lprdData->hRequest))
	{
		ProcessMultipartForm(lphedInfo->lprdData->hRequest);
		StringConcatenateSafe(csArgs, " --multipart-form 1", sizeof(csArgs) - 1);

		for (
			ualIndex = 0;
			ualIndex < VectorSize(lphedInfo->lprdData->hvFormData);
			ualIndex++
		){
			LPFORM_DATA lpfdData;
			lpfdData = VectorAt(lphedInfo->lprdData->hvFormData, ualIndex);

			if (NULLPTR == lpfdData)
			{ continue; }

			StringConcatenateSafe(csArgs, " ", sizeof(csArgs) - 1);
			StringConcatenateSafe(csArgs, "\"--", sizeof(csArgs) - 1);
			StringConcatenateSafe(csArgs, lpfdData->lpszKey, sizeof(csArgs) - 1);
			StringConcatenateSafe(csArgs, "\" \"", sizeof(csArgs) - 1);
			StringConcatenateSafe(csArgs, lpfdData->lpszValue, sizeof(csArgs) - 1);
			StringConcatenateSafe(csArgs, "\"", sizeof(csArgs) - 1);	
		}
	}
	
	PrintFormat("%s %s\n", csProgram, csArgs);
	
	hProcess = CreateProcess(
		csProgram,
		csArgs,
		FALSE
	);

	WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);
	DestroySplitString(dlpszSplit);
	DestroyObject(hProcess);
	return NULLPTR;
}
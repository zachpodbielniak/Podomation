/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <PodNet/CNetworking/CSmtp.h>
#include <PodNet/CProcess/CProcess.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>




PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded Weather Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}




PODOMATION_API
LPVOID
Email(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	lpppInfo = (LPPODPARAM)lpParam;
	
	HANDLE hSmtp;
	HANDLE hProcess;
	HANDLE hOutput;
	CSTRING csOutput[512];
	CSTRING csFrom[512];
	CSTRING csTo[512];
	CSTRING csSubject[512];
	CSTRING csBody[8192];
	LPSTR lpszOutput;
	DLPSTR dlpszSplit;
	UARCHLONG ualCount;

	ZeroMemory(csFrom, sizeof(csFrom));
	ZeroMemory(csTo, sizeof(csTo));
	ZeroMemory(csSubject, sizeof(csSubject));
	ZeroMemory(csBody, sizeof(csBody));

	ualCount = StringSplit(
		HeapStringValue(lpppInfo->hsParam),
		",",
		&dlpszSplit
	);

	if (3 != ualCount)
	{ 
		DestroySplitString(dlpszSplit);
		WriteFile(GetStandardOut(), ANSI_RED "Email() was not called with 3 parameters!\n" ANSI_RESET, 0);

		DestroyObject(GetState()->hLog);
		PostQuitMessage(2);
	}

	StringCopySafe(csFrom, dlpszSplit[0], sizeof(csFrom) - 1);
	StringCopySafe(csTo, dlpszSplit[1], sizeof(csTo) - 1);
	StringCopySafe(csSubject, dlpszSplit[2], sizeof(csSubject) - 1);

	hProcess = CreateProcess(
		"/usr/bin/python3",
		"./Scripts/DailyWord",
		FALSE
	);

	lpszOutput = (LPSTR)csOutput;
	hOutput = GetProcessOutput(hProcess);
	ZeroMemory(csOutput, sizeof(csOutput));
	WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);

	while (-1 != ReadLineFromFile(hOutput, &lpszOutput, sizeof(csOutput) - 1, 0))
	{
		StringConcatenateSafe(csBody, csOutput, sizeof(csBody) - 1);
		ZeroMemory(csOutput, sizeof(csOutput));
	}

	
	hSmtp = CreateSmtpConnection(
		SMTP_PROTOCOL_SSL,
		"disroot.org",
		NULLPTR,
		NULLPTR,
		TRUE,
		FALSE
	);

	SendEmail(
		hSmtp,
		csFrom,
		csTo,
		NULLPTR,
		csSubject,
		csBody
	);

	DestroyObject(hSmtp);
	DestroyObject(hProcess);
	return NULLPTR;
}




PODOMATION_API
LPVOID
Gotify(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	lpppInfo = (LPPODPARAM)lpParam;
	
	HANDLE hProcess;
	HANDLE hOutput;
	CSTRING csOutput[512];
	CSTRING csBody[8192];
	CSTRING csParams[8192];
	LPSTR lpszOutput;

	ZeroMemory(csBody, sizeof(csBody));
	ZeroMemory(csParams, sizeof(csParams));

	hProcess = CreateProcess(
		"/usr/bin/python3",
		"./Scripts/DailyWord",
		FALSE
	);

	lpszOutput = (LPSTR)csOutput;
	hOutput = GetProcessOutput(hProcess);
	ZeroMemory(csOutput, sizeof(csOutput));
	WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);

	while (-1 != ReadLineFromFile(hOutput, &lpszOutput, sizeof(csOutput) - 1, 0))
	{
		StringConcatenateSafe(csBody, csOutput, sizeof(csBody) - 1);
		ZeroMemory(csOutput, sizeof(csOutput));
	}
	DestroyObject(hProcess);

	StringPrintFormatSafe(
		csParams,
		sizeof(csParams) - 1,
		"./Scripts/GotifySend \"%s\" \"Daily Word\" \"%s\"",
		HeapStringValue(lpppInfo->hsParam),
		csBody
	);

	hProcess = CreateProcess(
		"/usr/bin/bash",
		csParams,
		FALSE
	);

	WaitForSingleObject(hProcess, INFINITE_WAIT_TIME);
	DestroyObject(hProcess);
	return NULLPTR;
}

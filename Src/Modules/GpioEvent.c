/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>


typedef struct __GPIO_EVENT
{
	HANDLE 		hGpio;
	HANDLE 		hThread;

	LPPODREGISTER	lpprOnStateChange;
	LPPODREGISTER	lpprOnHighState;
	LPPODREGISTER	lpprOnLowState;

	BOOL 		bState;
} GPIO_EVENT, *LPGPIO_EVENT;




PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded GpioEvent Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
__GpioProc(
	_In_Opt_ 	LPVOID 		lpParam
){
	LPPODPARAM lpppInfo;
	LPGPIO_EVENT lpgeInfo;
	BOOL bState;

	lpppInfo = (LPPODPARAM)lpParam;
	lpgeInfo = *(lpppInfo->dlpUserData);

	INFINITE_LOOP()
	{
		bState = ReadGpio(
			lpgeInfo->hGpio,
			NULLPTR,
			GPIO_INTERLOCKED_OPERATION
		);

		if (bState != lpgeInfo->bState)
		{
			if (NULLPTR != lpgeInfo->lpprOnStateChange)
			{
				SignalEvent(lpgeInfo->lpprOnStateChange->hEvent);
				ResetEvent(lpgeInfo->lpprOnStateChange->hEvent);
			}

			if (GPIO_OFF == bState)
			{
				if (NULLPTR != lpgeInfo->lpprOnLowState)
				{
					SignalEvent(lpgeInfo->lpprOnLowState->hEvent);
					ResetEvent(lpgeInfo->lpprOnLowState->hEvent);
				}
			}
			else
			{
				if (NULLPTR != lpgeInfo->lpprOnHighState)
				{
					SignalEvent(lpgeInfo->lpprOnHighState->hEvent);
					ResetEvent(lpgeInfo->lpprOnHighState->hEvent);
				}
			}
			
		}

		lpgeInfo->bState = bState;

		/* Real Time */
		Sleep(16);
	}

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
RegisterEvent(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODREGISTER lpprInfo;
	LPGPIO_EVENT lpgeInfo;

	lpprInfo = (LPPODREGISTER)lpParam;
	lpgeInfo = *(lpprInfo->dlpUserData);

	/* Parse Type */
	if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnStateChange"
	)){ lpgeInfo->lpprOnStateChange = lpprInfo; }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnHighState"
	)){ lpgeInfo->lpprOnHighState = lpprInfo; }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnLowState"
	)){ lpgeInfo->lpprOnLowState = lpprInfo; }
	else
	{
		CSTRING csBuffer[8192];
		ZeroMemory(csBuffer, sizeof(csBuffer));

		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			"%s is not a valid GpioEvent!\n",
			HeapStringValue(lpprInfo->hsEvent)
		);

		WriteFile(GetStandardError(), ANSI_RED, 0);
		WriteFile(GetStandardError(), csBuffer, 0);
		WriteFile(GetStandardError(), ANSI_RESET, 0);

		WriteLog(
			GetState()->hLog,
			csBuffer,
			LOG_FATAL,
			0
		);

		PostQuitMessage(1);
	}
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID 
New(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODPARAM lpppPod;
	LPGPIO_EVENT lpgeInfo;
	ULONG ulPin;

	lpppPod = (LPPODPARAM)lpParam;
	lpgeInfo = GlobalAllocAndZero(sizeof(GPIO_EVENT));

	StringScanFormat(
		HeapStringValue(lpppPod->hsParam),
		"%lu",
		&ulPin
	);

	lpgeInfo->hGpio = OpenGpioPinAndSpecifyMode(
		ulPin, 
		GPIO_MODE_INPUT, 
		GPIO_PULL_MODE_OFF
	);

	lpgeInfo->bState = ReadGpio(
		lpgeInfo->hGpio,
		NULLPTR,
		GPIO_INTERLOCKED_OPERATION
	);

	*(lpppPod->dlpUserData) = (LPVOID)lpgeInfo;

	lpgeInfo->hThread = CreateThread(__GpioProc, lpppPod, NULLPTR);
	return NULLPTR;
}
/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef INOTIFY_EVENT_DATA_H
#define INOFITY_EVENT_DATA_H


#include <PodNet/PodNet.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>
#include <sys/inotify.h>




typedef struct __INOTIFY_EVENT
{
	HANDLE 		hThread;
	HSTRING		hsDir;
	LONG 		lFd;

	HVECTOR 	hvOnModify;
	HVECTOR 	hvOnAttrib;
	HVECTOR 	hvOnAccess;
	HVECTOR 	hvOnCloseWrite;
	HVECTOR 	hvOnCloseNoWrite;
	HVECTOR 	hvOnOpen;
	HVECTOR		hvOnMovedFrom;
	HVECTOR 	hvOnMovedTo;
	HVECTOR 	hvOnCreate;
	HVECTOR 	hvOnDelete;
	HVECTOR 	hvOnDeleteSelf;
	HVECTOR 	hvOnMoveSelf;
	HVECTOR 	hvOnStart;
} INOTIFY_EVENT, *LPINOTIFY_EVENT;


typedef struct __INOTIFY_EVENT_DATA
{
	LPINOTIFY_EVENT		lpieData;
	struct inotify_event 	*lpieEvent;
	UARCHLONG 		ualEventType;
} INOTIFY_EVENT_DATA, *LPINOTIFY_EVENT_DATA;



#endif 
/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include <PodNet/PodNet.h>
#include <sys/inotify.h>
#include <Podomation/Pod/Pod.h>
#include <Podomation/State/State.h>
#include "InotifyEventData.h"




PODOMATION_MODULE
LPVOID
OnCreate(
	_In_ 		HANDLE 			hModule,
	_In_Opt_ 	LPVOID 			lpParam
){
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(lpParam);

	WriteLog(
		GetState()->hLog,
		"Loaded InotifyEvent Module\n",
		LOG_INFO,
		0
	);

	return NULLPTR;
}




PODOMATION_MODULE
LPVOID
__InotifyProc(
	_In_Opt_	LPVOID 			lpParam
){
	LPPODPARAM lpppInfo;
	LPINOTIFY_EVENT lpieInfo;
	LPPODREGISTER lpprIndex;
	UARCHLONG ualIndex;
	CHAR cBuf[16384] __attribute__ ((aligned(__alignof__(struct inotify_event))));
	struct inotify_event *lpieEvt;
	LPVOID lpData;
	ARCHLONG alLength;
	INOTIFY_EVENT_DATA iedArg;

	lpppInfo = (LPPODPARAM)lpParam;
	lpieInfo = *(lpppInfo->dlpUserData);

	for (
		ualIndex = 0;
		ualIndex < VectorSize(lpieInfo->hvOnStart);
		ualIndex++
	){
		lpprIndex = VectorAt(lpieInfo->hvOnStart, ualIndex);

		if (NULLPTR == lpprIndex)
		{ continue; }

		SignalEvent(lpprIndex->hEvent);
		ResetEvent(lpprIndex->hEvent);
	}

	INFINITE_LOOP()
	{
		__START_LOOP:
		alLength = read(lpieInfo->lFd, cBuf, sizeof(cBuf));
		if (-1 == alLength)
		{ 
			WriteFile(GetStandardError(), "Read Failure On INOTIFY_EVENT\n", 0);
			PostQuitMessage(3);
		}

		if (0 == alLength)
		{ continue; }

		for (
			lpData = cBuf;
			(UARCHLONG)lpData < (UARCHLONG)((UARCHLONG)cBuf + alLength);
			lpData += sizeof(struct inotify_event)
		){
			lpieEvt = (struct inotify_event *)lpData;
			lpData = (LPVOID)(((UARCHLONG)lpData) + (UARCHLONG)lpieEvt->len);

			/* Callee Args */
			iedArg.lpieData = lpieInfo;
			iedArg.lpieEvent = lpieEvt;
			*(lpppInfo->dlpCalleeData) = &iedArg;

			if (IN_ACCESS & lpieEvt->mask)
			{
				for (
					ualIndex = 0;
					ualIndex < VectorSize(lpieInfo->hvOnAccess);
					ualIndex++
				){
					lpprIndex = *(LPPODREGISTER *)VectorAt(lpieInfo->hvOnAccess, ualIndex);

					if (NULLPTR == lpprIndex)
					{ continue; }

					iedArg.ualEventType = IN_ACCESS;
					SignalEvent(lpprIndex->hEvent);
					ResetEvent(lpprIndex->hEvent);
				}
			}
			
			if (IN_ATTRIB & lpieEvt->mask)
			{
				for (
					ualIndex = 0;
					ualIndex < VectorSize(lpieInfo->hvOnAttrib);
					ualIndex++
				){
					lpprIndex = *(LPPODREGISTER *)VectorAt(lpieInfo->hvOnAttrib, ualIndex);

					if (NULLPTR == lpprIndex)
					{ continue; }

					iedArg.ualEventType = IN_ATTRIB;
					SignalEvent(lpprIndex->hEvent);
					ResetEvent(lpprIndex->hEvent);
				}
			}
			
			if (IN_CLOSE_NOWRITE & lpieEvt->mask)
			{
				for (
					ualIndex = 0;
					ualIndex < VectorSize(lpieInfo->hvOnCloseNoWrite);
					ualIndex++
				){
					lpprIndex = *(LPPODREGISTER *)VectorAt(lpieInfo->hvOnCloseNoWrite, ualIndex);

					if (NULLPTR == lpprIndex)
					{ continue; }

					iedArg.ualEventType = IN_CLOSE_NOWRITE;
					SignalEvent(lpprIndex->hEvent);
					ResetEvent(lpprIndex->hEvent);
				}
			}
			
			if (IN_CLOSE_WRITE & lpieEvt->mask)
			{
				for (
					ualIndex = 0;
					ualIndex < VectorSize(lpieInfo->hvOnCloseWrite);
					ualIndex++
				){
					lpprIndex = *(LPPODREGISTER *)VectorAt(lpieInfo->hvOnCloseWrite, ualIndex);

					if (NULLPTR == lpprIndex)
					{ continue; }

					iedArg.ualEventType = IN_CLOSE_WRITE;
					SignalEvent(lpprIndex->hEvent);
					ResetEvent(lpprIndex->hEvent);
				}
			}
			
			if (IN_CREATE & lpieEvt->mask)
			{
				for (
					ualIndex = 0;
					ualIndex < VectorSize(lpieInfo->hvOnCreate);
					ualIndex++
				){
					lpprIndex = *(LPPODREGISTER *)VectorAt(lpieInfo->hvOnCreate, ualIndex);

					if (NULLPTR == lpprIndex)
					{ continue; }

					iedArg.ualEventType = IN_CREATE;
					SignalEvent(lpprIndex->hEvent);
					ResetEvent(lpprIndex->hEvent);
				}
			}
			
			if (IN_DELETE_SELF & lpieEvt->mask)
			{
				for (
					ualIndex = 0;
					ualIndex < VectorSize(lpieInfo->hvOnDeleteSelf);
					ualIndex++
				){
					lpprIndex = *(LPPODREGISTER *)VectorAt(lpieInfo->hvOnDeleteSelf, ualIndex);

					if (NULLPTR == lpprIndex)
					{ continue; }

					iedArg.ualEventType = IN_DELETE_SELF;
					SignalEvent(lpprIndex->hEvent);
					ResetEvent(lpprIndex->hEvent);
				}
			} 
			
			if (IN_DELETE & lpieEvt->mask)
			{
				for (
					ualIndex = 0;
					ualIndex < VectorSize(lpieInfo->hvOnDelete);
					ualIndex++
				){
					lpprIndex = *(LPPODREGISTER *)VectorAt(lpieInfo->hvOnDelete, ualIndex);

					if (NULLPTR == lpprIndex)
					{ continue; }

					iedArg.ualEventType = IN_DELETE;
					SignalEvent(lpprIndex->hEvent);
					ResetEvent(lpprIndex->hEvent);
				}
			} 
			
			if (IN_MODIFY & lpieEvt->mask)
			{
				for (
					ualIndex = 0;
					ualIndex < VectorSize(lpieInfo->hvOnModify);
					ualIndex++
				){
					lpprIndex = *(LPPODREGISTER *)VectorAt(lpieInfo->hvOnModify, ualIndex);

					if (NULLPTR == lpprIndex)
					{ continue; }

					iedArg.ualEventType = IN_MODIFY;
					SignalEvent(lpprIndex->hEvent);
					ResetEvent(lpprIndex->hEvent);
				}
			} 
			
			if (IN_MOVE_SELF & lpieEvt->mask)
			{
				for (
					ualIndex = 0;
					ualIndex < VectorSize(lpieInfo->hvOnMoveSelf);
					ualIndex++
				){
					lpprIndex = *(LPPODREGISTER *)VectorAt(lpieInfo->hvOnMoveSelf, ualIndex);

					if (NULLPTR == lpprIndex)
					{ continue; }

					iedArg.ualEventType = IN_MOVE_SELF;
					SignalEvent(lpprIndex->hEvent);
					ResetEvent(lpprIndex->hEvent);
				}
			} 
			
			if (IN_MOVED_FROM & lpieEvt->mask)
			{
				for (
					ualIndex = 0;
					ualIndex < VectorSize(lpieInfo->hvOnMovedFrom);
					ualIndex++
				){
					lpprIndex = *(LPPODREGISTER *)VectorAt(lpieInfo->hvOnMovedFrom, ualIndex);

					if (NULLPTR == lpprIndex)
					{ continue; }

					iedArg.ualEventType = IN_MOVED_FROM;
					SignalEvent(lpprIndex->hEvent);
					ResetEvent(lpprIndex->hEvent);
				}
			} 
			
			if (IN_MOVED_TO & lpieEvt->mask)
			{
				for (
					ualIndex = 0;
					ualIndex < VectorSize(lpieInfo->hvOnMovedTo);
					ualIndex++
				){
					lpprIndex = *(LPPODREGISTER *)VectorAt(lpieInfo->hvOnMovedTo, ualIndex);

					if (NULLPTR == lpprIndex)
					{ continue; }

					iedArg.ualEventType = IN_MOVED_TO;
					SignalEvent(lpprIndex->hEvent);
					ResetEvent(lpprIndex->hEvent);
				}
			} 
			
			if (IN_OPEN & lpieEvt->mask)
			{
				for (
					ualIndex = 0;
					ualIndex < VectorSize(lpieInfo->hvOnOpen);
					ualIndex++
				){
					lpprIndex = *(LPPODREGISTER *)VectorAt(lpieInfo->hvOnOpen, ualIndex);

					if (NULLPTR == lpprIndex)
					{ continue; }

					iedArg.ualEventType = IN_OPEN;
					SignalEvent(lpprIndex->hEvent);
					ResetEvent(lpprIndex->hEvent);
				}
			} 

			/* JUMP(__START_LOOP); */
		}
	}
}




PODOMATION_MODULE
LPVOID
RegisterEvent(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODREGISTER lpprInfo;
	LPINOTIFY_EVENT lpieInfo;
	DLPSTR dlpszSplit;
	UARCHLONG ualCount;

	lpprInfo = (LPPODREGISTER)lpParam;
	lpieInfo = *(lpprInfo->dlpUserData);

	/* Parse Type */
	if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnStart"
	)){ VectorPushBack(lpieInfo->hvOnStart, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnModify"
	)){ VectorPushBack(lpieInfo->hvOnModify, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnAccess"
	)){ VectorPushBack(lpieInfo->hvOnAccess, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnCloseWrite"
	)){ VectorPushBack(lpieInfo->hvOnCloseWrite, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnCloseNoWrite"
	)){ VectorPushBack(lpieInfo->hvOnCloseNoWrite, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnOpen"
	)){ VectorPushBack(lpieInfo->hvOnOpen, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnMovedFrom"
	)){ VectorPushBack(lpieInfo->hvOnMovedFrom, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnMovedTo"
	)){ VectorPushBack(lpieInfo->hvOnMovedTo, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnCreate"
	)){ VectorPushBack(lpieInfo->hvOnCreate, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnDelete"
	)){ VectorPushBack(lpieInfo->hvOnDelete, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnDeleteSelf"
	)){ VectorPushBack(lpieInfo->hvOnDeleteSelf, &lpprInfo, 0); }
	else if (0 == StringCompare(
		HeapStringValue(lpprInfo->hsEvent),
		"OnMoveSelf"
	)){ VectorPushBack(lpieInfo->hvOnMoveSelf, &lpprInfo, 0); }
	else
	{
		/* Error */
		WriteFile(
			GetStandardError(),
			"InotifyEvent Does Not Recognize This Event!\n",
			0
		);
		PostQuitMessage(3);
	}
	return NULLPTR;
}




PODOMATION_MODULE
LPVOID 
New(
	_In_ 		HANDLE 		hModule,
	_In_Opt_ 	LPVOID 		lpParam
){
	UNREFERENCED_PARAMETER(hModule);

	LPPODPARAM lpppPod;
	LPINOTIFY_EVENT lpieInfo;
	UARCHLONG ualCount;

	lpppPod = (LPPODPARAM)lpParam;
	lpieInfo = GlobalAllocAndZero(sizeof(INOTIFY_EVENT));

	lpieInfo->lFd = inotify_init();
	inotify_add_watch(
		lpieInfo->lFd,
		HeapStringValue(lpppPod->hsParam),
		IN_ALL_EVENTS
	);

	lpieInfo->hvOnAttrib = CreateVector(0x10, sizeof(LPPODREGISTER), 0);
	lpieInfo->hvOnCloseNoWrite = CreateVector(0x10, sizeof(LPPODREGISTER), 0);
	lpieInfo->hvOnCloseWrite = CreateVector(0x10, sizeof(LPPODREGISTER), 0);
	lpieInfo->hvOnCreate = CreateVector(0x10, sizeof(LPPODREGISTER), 0);
	lpieInfo->hvOnDelete = CreateVector(0x10, sizeof(LPPODREGISTER), 0);
	lpieInfo->hvOnDeleteSelf = CreateVector(0x10, sizeof(LPPODREGISTER), 0);
	lpieInfo->hvOnModify = CreateVector(0x10, sizeof(LPPODREGISTER), 0);
	lpieInfo->hvOnMovedFrom = CreateVector(0x10, sizeof(LPPODREGISTER), 0);
	lpieInfo->hvOnMovedTo = CreateVector(0x10, sizeof(LPPODREGISTER), 0);
	lpieInfo->hvOnMoveSelf = CreateVector(0x10, sizeof(LPPODREGISTER), 0);
	lpieInfo->hvOnOpen = CreateVector(0x10, sizeof(LPPODREGISTER), 0);
	lpieInfo->hvOnStart = CreateVector(0x10, sizeof(LPPODREGISTER), 0);

	*(lpppPod->dlpUserData) = (LPVOID)lpieInfo;

	lpieInfo->hThread = CreateThread(__InotifyProc, lpppPod, NULLPTR);
	return NULLPTR;
}

/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "ConfigParser.h"
#include "KeyWords.h"



typedef enum __TOKEN_TYPE
{
	TOKEN_TYPE_UNKNOWN		= 0,
	TOKEN_TYPE_EQUALS,
	TOKEN_TYPE_POINTS_TO,
	TOKEN_TYPE_END
} TOKEN_TYPE;


GLOBAL_VARIABLE LPSTR dlpszSplitters[] = {
	" ", "\t", "\n", "=", ";", "->"
};


TOKEN lptknData[] = {
	{"=", TOKEN_TYPE_EQUALS},
	{";", TOKEN_TYPE_END},
	{"->", TOKEN_TYPE_POINTS_TO}
};



typedef enum __PARSE_STATE
{
	STATE_LITERAL		= 0,
	STATE_OPEN_PAREN,
	STATE_CLOSE_PAREN,
	STATE_EQUAL,
	STATE_END,

	/* Assignment */
	STATE_TABLE_SIZE,
	STATE_TABLE_SIZE_EQUAL,
	STATE_POD_DECLARATION_BEGIN,
	STATE_POD_DECLARATION_KEY,
	STATE_POD_DECLARATION_EQUAL,
	STATE_POD_DECLARATION_VALUE,
	
	/* Event Specifying */
	STATE_POD_EVENT_BEGIN,
	STATE_POD_EVENT_POINTS_TO,
	STATE_POD_EVENT_KEY,
	STATE_POD_EVENT_EQUAL,
	STATE_POD_EVENT_VALUE,

	STATE_ERROR		= MAX_ULONG
} PARSE_STATE;




INTERNAL_OPERATION
VOID
__ReadConfig(
	VOID
){
	UARCHLONG ualFileSize;
	ualFileSize = GetFileSize(GetState()->hConfigFile);
	GetState()->lpszConfig = GlobalAllocAndZero(ualFileSize + 1);
	ReadWholeFile(GetState()->hConfigFile, &(GetState()->lpszConfig), 0);
}



_Success_(return != (UARCHLONG)-1, _Non_Locking_)
INTERNAL_OPERATION
UARCHLONG
__GetPodKey(
	_In_ 		LPCSTR 		lpcszName,
	_Out_Opt_ 	LPHANDLE 	lphPod
){
	HANDLE hPod;
	LEXEME lxCompare;
	UARCHLONG ualKey;

	ualKey = HashTableGetKey(
		GetState()->hhtPods, 
		(ULONGLONG)GetState()->csPod, 
		StringLength(GetState()->csPod), 
		FALSE
	);

	lxCompare = (LEXEME)HashTableGetValueEx(
		GetState()->hhtPods,
		ualKey,
		NULLPTR,
		NULLPTR,
		(LPULONGLONG)&hPod
	);

	/* Verify Data From HashTable */
	if (
		NULLPTR != lxCompare && 
		0 == StringCompare(lpcszName, lxCompare) &&
		NULL_OBJECT != hPod
	){ NO_OPERATION(); }
	else
	{ ualKey = (UARCHLONG)-1; }
	
	if (NULLPTR != lphPod)
	{ *lphPod = hPod; }

	return ualKey;
}




/* TODO: Parse this, and determine if its a loadable module or not */
INTERNAL_OPERATION
HANDLE 
__ParsePodValue(
	_In_Z_ 		LPSTR 		lpszValue
){
	HANDLE hPod;
	hPod = CreatePod(lpszValue, NULLPTR, 0);
	return hPod;
}




INTERNAL_OPERATION
MACHINESTATE
__ParserEqual(
	_In_		HANDLE 		hParser,
	_In_ 		MACHINESTATE	msCurrent,
	_In_		LEXEME 		lxLeft,
	_In_ 		LEXEME 		lxRight,
	_In_ 		TOKENFLAG 	tfType,
	_In_Opt_ 	LPVOID 		lpData
){
	UNREFERENCED_PARAMETER(hParser);
	UNREFERENCED_PARAMETER(lxLeft);
	UNREFERENCED_PARAMETER(lxRight);
	UNREFERENCED_PARAMETER(tfType);
	UNREFERENCED_PARAMETER(lpData);

	MACHINESTATE msNew;
	msNew = msCurrent;

	switch ((UARCHLONG)msCurrent)
	{
		case STATE_TABLE_SIZE:
		{
			msNew = STATE_TABLE_SIZE_EQUAL;
			break;
		}

		case STATE_POD_DECLARATION_KEY:
		{
			msNew = STATE_POD_DECLARATION_EQUAL;
			break;
		}

		/* Not Sure Why? */
		case STATE_POD_EVENT_POINTS_TO:
		case STATE_POD_EVENT_KEY:
		{
			msNew = STATE_POD_EVENT_EQUAL;
			break;
		}

		default:
		{ break; }
	}

	return msNew;
}




INTERNAL_OPERATION
MACHINESTATE
__ParserPointsTo(
	_In_		HANDLE 		hParser,
	_In_ 		MACHINESTATE	msCurrent,
	_In_		LEXEME 		lxLeft,
	_In_ 		LEXEME 		lxRight,
	_In_ 		TOKENFLAG 	tfType,
	_In_Opt_ 	LPVOID 		lpData
){
	UNREFERENCED_PARAMETER(hParser);
	UNREFERENCED_PARAMETER(lxLeft);
	UNREFERENCED_PARAMETER(lxRight);
	UNREFERENCED_PARAMETER(tfType);
	UNREFERENCED_PARAMETER(lpData);

	MACHINESTATE msNew;
	msNew = msCurrent;

	switch ((UARCHLONG)msCurrent)
	{
		case STATE_POD_EVENT_BEGIN:
		{
			msNew = STATE_POD_EVENT_POINTS_TO;
			break;
		}
	}

	return msNew;
}




INTERNAL_OPERATION
MACHINESTATE
__ParserEnd(
	_In_		HANDLE 		hParser,
	_In_ 		MACHINESTATE	msCurrent,
	_In_		LEXEME 		lxLeft,
	_In_ 		LEXEME 		lxRight,
	_In_ 		TOKENFLAG 	tfType,
	_In_Opt_ 	LPVOID 		lpData
){
	UNREFERENCED_PARAMETER(hParser);
	UNREFERENCED_PARAMETER(lxLeft);
	UNREFERENCED_PARAMETER(lxRight);
	UNREFERENCED_PARAMETER(tfType);
	UNREFERENCED_PARAMETER(lpData);

	MACHINESTATE msNew;
	msNew = msCurrent;

	switch ((UARCHLONG)msCurrent)
	{
		default:
		{
			msNew = STATE_END;
		}
	}

	return msNew;
}




INTERNAL_OPERATION
MACHINESTATE
__ParserLiteral(
	_In_		HANDLE 		hParser,
	_In_ 		MACHINESTATE	msCurrent,
	_In_		LEXEME 		lxLeft,
	_In_ 		LEXEME 		lxRight,
	_In_ 		TOKENFLAG 	tfType,
	_In_Opt_ 	LPVOID 		lpData
){
	UNREFERENCED_PARAMETER(hParser);
	UNREFERENCED_PARAMETER(lxRight);
	UNREFERENCED_PARAMETER(lpData);
	UNREFERENCED_PARAMETER(tfType);

	MACHINESTATE msNew;
	CSTRING csBuffer[8192];

	msNew = msCurrent;

	if ('\0' == *lxLeft && '\0' == *lxRight)
	{ return msCurrent; }

	ZeroMemory(csBuffer, sizeof(csBuffer));

	switch((UARCHLONG)msCurrent)
	{
		case STATE_CLOSE_PAREN:
		case STATE_OPEN_PAREN:
		case STATE_LITERAL:
		{ break; }

		/* New Statements Here (Typically A New Line) */
		case STATE_END:
		{
			UARCHLONG ualKey;

			/* Declaration of a new POD */
			if (0 == StringCompare(lxLeft, KEYWORD_POD_DECLARATION))
			{ msNew = STATE_POD_DECLARATION_BEGIN; }

			/* Specifying an event */
			else if ((UARCHLONG)-1 != (ualKey = HashTableGetKey(GetState()->hhtPods, (ULONGLONG)lxLeft, StringLength(lxLeft), FALSE)))
			{ 
				LEXEME lxCompare;
				lxCompare = (LEXEME)HashTableGetValue(
					GetState()->hhtPods,
					ualKey,
					NULLPTR,
					NULLPTR
				);

				if (
					NULLPTR != lxCompare && 
					0 == StringCompare(lxLeft, lxCompare)
				){ 
					ZeroMemory(GetState()->csPod, 8192);
					CopyMemory(GetState()->csPod, lxLeft, StringLength(lxLeft));

					msNew = STATE_POD_EVENT_BEGIN; 
				}
			}

			break;
		}

		/* Store in the CarryOver for STATE_POD_DECLARATION_EQUAL */
		case STATE_POD_DECLARATION_BEGIN:
		{
			StringScanFormat(lxLeft, "%s", csBuffer);
			ZeroMemory(GetState()->csCarryOver, 8192);
			CopyMemory(GetState()->csCarryOver, csBuffer, sizeof(csBuffer));
			msNew = STATE_POD_DECLARATION_KEY;
			break;
		}

		/* Ready to insert into HashTable */
		case STATE_POD_DECLARATION_EQUAL:
		{
			HANDLE hPod; 
			LPSTR lpszAlloc;
			UARCHLONG ualSize;

			StringPrintFormatSafe(
				csBuffer,
				sizeof(csBuffer) - 1,
				"POD %s is declared with value of %s.\n",
				GetState()->csCarryOver,
				lxLeft
			);

			WriteLog(
				GetState()->hLog,
				csBuffer,
				LOG_INFO,
				0
			);

			/* Parse The Value */
			hPod = __ParsePodValue(lxLeft);

			/* Allocate Copy And Insert Into HashTable */
			ualSize = StringLength(GetState()->csCarryOver);
			lpszAlloc = GlobalAllocAndZero(ualSize + 1);
			StringCopySafe(lpszAlloc, GetState()->csCarryOver, ualSize);

			HashTableInsertEx(
				GetState()->hhtPods,
				(ULONGLONG)lpszAlloc,
				StringLength(GetState()->csCarryOver),
				FALSE,
				(ULONGLONG)hPod
			);

			break;
		}

		/* Copy To CarryOver For STATE_POD_EVENT_EQUAL */
		case STATE_POD_EVENT_POINTS_TO:
		{
			StringScanFormat(
				lxLeft,
				"%s",
				csBuffer
			);

			ZeroMemory(GetState()->csCarryOver, 8192);
			CopyMemory(GetState()->csCarryOver, csBuffer, sizeof(csBuffer));

			msCurrent = STATE_POD_EVENT_KEY;
			break;
		}

		/* Register the event in the POD */
		case STATE_POD_EVENT_EQUAL:
		{
			UARCHLONG ualKey;
			HANDLE hPod; 

			StringPrintFormat(
				csBuffer,
				"Registering %s event with value %s for POD %s\n",
				GetState()->csCarryOver,
				lxLeft,
				GetState()->csPod
			);

			WriteLog(
				GetState()->hLog,
				csBuffer,
				LOG_INFO,
				0
			);

			ualKey = __GetPodKey(GetState()->csPod, &hPod);
			
			/* Write Log And Quit */
			if ((UARCHLONG)-1 == ualKey)
			{
				ZeroMemory(csBuffer, sizeof(csBuffer));
				StringPrintFormatSafe(
					csBuffer,
					sizeof(csBuffer) - 1,
					"Could not get key index for POD %s\n",
					GetState()->csPod	
				);

				WriteLog(
					GetState()->hLog,
					csBuffer,
					LOG_ERROR,
					0
				);
				
				WriteFile(GetStandardError(), ANSI_RED, 0);
				WriteFile(GetStandardError(), csBuffer, 0);
				WriteFile(GetStandardError(), ANSI_RESET, 0);

				DestroyObject(GetState()->hLog);
				PostQuitMessage(1);
			}
			/* Insert Event */
			else
			{
				LPSTR lpszAlloc;
				UARCHLONG ualSize;

				/* Allocate And Insert */
				ualSize = StringLength(GetState()->csCarryOver);
				lpszAlloc = GlobalAllocAndZero(ualSize + 1); /* TODO: Needs A Call To FreeMemory() */
				StringCopySafe(lpszAlloc, GetState()->csCarryOver, ualSize);

				RegisterPodEvent(
					hPod,
					lpszAlloc,
					lxLeft,
					NULLPTR
				);
			}
			
			msCurrent = STATE_POD_EVENT_VALUE;
			break;
		}

		case STATE_TABLE_SIZE_EQUAL:
		{
			UARCHLONG ualX;

			StringScanFormat(lxLeft, "%lu", &ualX);
			StringPrintFormatSafe(
				csBuffer,
				sizeof(csBuffer) - 1,
				"Creating HASHTABLE of size %lu.\n",
				ualX
			);
			
			WriteLog(
				GetState()->hLog,
				csBuffer,
				LOG_INFO,
				0
			);

			GetState()->hhtPods = CreateHashTable(ualX);
			break;
		}

		/* Define The Size Of The Hash Table */
		case MACHINE_STATE_NULL:
		{	
			if (0 != StringCompare(lxLeft, KEYWORD_HASHTABLE_SIZE))
			{
				WriteLog(
					GetState()->hLog, 
					"TableSize must be the first entry in the config file.\n", 
					LOG_ERROR, 
					0
				);

				WriteFile(GetStandardError(), ANSI_RED, 0);
				WriteFile(GetStandardError(), "First entry in config file must be " KEYWORD_HASHTABLE_SIZE ".\n", 0);
				WriteFile(GetStandardError(), ANSI_RESET, 0);

				DestroyObject(GetState()->hLog);
				PostQuitMessage(1);
			}

			msNew = STATE_TABLE_SIZE;
			break;
		}		
	}

	return msNew;
}



_Success_(return != FALSE, _Interlocked_Operation_)
PODOMATION_API
BOOL
ParseConfig(
	VOID
){
	GetState()->hLexer = CreateLexer(
		8192,
		dlpszSplitters,
		sizeof(dlpszSplitters) / sizeof(*dlpszSplitters)
	);

	GetState()->hTokenTable = CreateTokenTable(lptknData, (sizeof(lptknData) / sizeof(*lptknData)) + 0x01U);
	GetState()->hParser = CreateParser(GetState()->hLexer, GetState()->hTokenTable);

	ParserRegisterCallback(
		GetState()->hParser,
		TOKEN_TYPE_UNKNOWN,
		__ParserLiteral,
		NULLPTR
	);

	ParserRegisterCallback(
		GetState()->hParser,
		TOKEN_TYPE_EQUALS,
		__ParserEqual,
		NULLPTR
	);

	ParserRegisterCallback(
		GetState()->hParser,
		TOKEN_TYPE_END,
		__ParserEnd,
		NULLPTR
	);

	ParserRegisterCallback(
		GetState()->hParser,
		TOKEN_TYPE_POINTS_TO,
		__ParserPointsTo,
		NULLPTR
	);


	__ReadConfig();
	ParserSetText(GetState()->hParser, GetState()->lpszConfig);

	Parse(GetState()->hParser);
	return WaitForSingleObject(GetState()->hParser, INFINITE_WAIT_TIME);
}

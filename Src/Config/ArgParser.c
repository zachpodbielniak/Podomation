/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "ArgParser.h"




_Success_(return != FALSE, _Interlocked_Operation_)
PODOMATION_API
BOOL
ParseArgs(
	VOID
){
	UARCHLONG ualIterator;
	LPSTR lpszIterator, lpszParam;
	CSTRING csBuffer[8192];

	lpszIterator = NULLPTR;
	lpszParam = NULLPTR;

	for (
		ualIterator = 0;
		ualIterator < (UARCHLONG)GetState()->lArgCount;
		ualIterator++
	){
		ZeroMemory(csBuffer, sizeof(csBuffer));
		lpszIterator = GetState()->dlpszArgValues[ualIterator];

		if (
			ualIterator < (UARCHLONG)(GetState()->lArgCount -1) &&
			'-' != *(GetState()->dlpszArgValues[ualIterator + 1])
		){ lpszParam = GetState()->dlpszArgValues[ualIterator + 1]; }


		/* Config File */
		if (
			0 == StringCompare("-c", lpszIterator) ||
			0 == StringCompare("--config", lpszIterator)
		){
			
			GetState()->hConfigFile = OpenFile(lpszParam, FILE_PERMISSION_READ, 0);

			if (NULL_OBJECT == GetState()->hConfigFile)
			{
				StringPrintFormatSafe(
					csBuffer,
					sizeof(csBuffer) - 1,
					"Could not open config file %s.\n",
					lpszParam
				);

				WriteLog(
					GetState()->hLog,
					csBuffer,
					LOG_ERROR,
					0
				);
			}
			else
			{
				StringPrintFormatSafe(
					csBuffer,
					sizeof(csBuffer) - 1,
					"Opened config file %s.\n",
					lpszParam
				);

				WriteLog(
					GetState()->hLog,
					csBuffer,
					LOG_INFO,
					0
				);
			}
		}	

		

	}

	return TRUE;
}
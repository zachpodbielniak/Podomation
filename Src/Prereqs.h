/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#ifndef PODOMATION_PREREQS_H
#define PODOMATION_PREREQS_H


#include <PodNet/PodNet.h>
#include <PodNet/CContainers/CContainers.h>
#include <PodNet/CDevices/CGpio.h>
#include <PodNet/CDevices/CI2C.h>
#include <PodNet/CDevices/CSPI.h>
#include <PodNet/CDevices/CSerial.h>
#include <PodNet/CFile/CFile.h>
#include <PodNet/CLex/CParser.h>
#include <PodNet/CLock/CLock.h>
#include <PodNet/CLog/CLog.h>
#include <PodNet/CMath/CRandom.h>
#include <PodNet/CMemory/CMemory.h>
#include <PodNet/CModule/CModule.h>
#include <PodNet/CModule/CCompileModule.h>
#include <PodNet/CNetworking/CNetworking.h>
#include <PodNet/CPoller/CPoller.h>
#include <PodNet/CProcess/CProcess.h>
#include <PodNet/CString/CHeapString.h>
#include <PodNet/CString/CString.h>
#include <PodNet/CSystem/CSystem.h>
#include <PodNet/CSystem/CHardware.h>
#include <PodNet/CThread/CThread.h>


#endif
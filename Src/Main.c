/*


 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


*/


#include "Prereqs.h"
#include "TypeDefs.h"
#include "Defs.h"
#include "State/State.h"
#include "Config/ArgParser.h"
#include "Config/ConfigParser.h"
#include "Pod/Pod.h"


INTERNAL_OPERATION
VOID
__SignalHandler(
	_In_ 		LONG 		lSignal
){
	switch(lSignal)
	{
		case SIGINT:
		{
			DestroyObject(GetState()->hLog);
			PrintFormat("\n");
			PostQuitMessage(0);
		}
	}
}




LONG
Main(
	_In_Opt_ 	LONG 		lArgCount,
	_In_Opt_Z_ 	DLPSTR 		dlpszArgValues
){
	signal(SIGINT, __SignalHandler);

	GetState()->hGlobalLock = CreateCriticalSectionAndSpecifySpinCount(0xC00);
	WaitForSingleObject(GetState()->hGlobalLock, INFINITE_WAIT_TIME);

	GetState()->lArgCount = lArgCount;
	GetState()->dlpszArgValues = dlpszArgValues;
	GetState()->hLog = CreateLog("Podomation-Log", LOG_INFO);
	GetState()->hReady = CreateEvent(FALSE);

	WriteLog(
		GetState()->hLog,
		"Starting up Podomation!\n",
		LOG_INFO,
		0
	);

	/* Parse Args */
	if (FALSE == ParseArgs())
	{ 
		WriteFile(GetStandardError(), ANSI_RED, 0);
		WriteFile(GetStandardError(), "Unable to parse arguments!\n", 0);
		WriteFile(GetStandardError(), ANSI_RESET, 0);

		DestroyObject(GetState()->hLog);
		PostQuitMessage(1);
	}

	ParseConfig();	
	ReleaseSingleObject(GetState()->hGlobalLock);
	SignalEvent(GetState()->hReady);

	INFINITE_LOOP()
	{ LongSleep(INFINITE_WAIT_TIME); }
	DestroyObject(GetState()->hLog);
	return 0;
}

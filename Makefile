#  ____           _                       _   _             
# |  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
# | |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
# |  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
# |_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

# Modular Event-Based Handler In C.
# Copyright (C) 2019 Zach Podbielniak

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License # along with this program.  If not, see <https://www.gnu.org/licenses/>. # along with this program.  If not, see <https://www.gnu.org/licenses/>. 


CC = gcc
STD = -std=gnu89
WARNINGS = -Wall -Wextra -Wno-format-truncation -Wno-format-overflow
WARNINGS += -Wno-cast-function-type
DEFINES = -D _DEFAULT_SOURCE
DEFINES = -D _GNU_SOURCE

DEFINES_D = $(DEFINES)
DEFINES_D += -D __DEBUG__

OPTIMIZE = -O3 -funroll-loops -fstrict-aliasing
OPTIMIZE += -fstack-protector-strong
MARCH = -march=native
MTUNE = -mtune=native

CC_FLAGS = $(STD)
CC_FLAGS += $(WARNINGS)
CC_FLAGS += $(DEFINES)
CC_FLAGS += $(OPTIMIZE)
CC_FLAGS += $(MARCH)
CC_FLAGS += $(MTUNE)

CC_FLAGS_D = $(STD)
CC_FLAGS_D += $(WARNINGS)
CC_FLAGS_D += $(DEFINES_D)

CC_FLAGS_T = $(CC_FLAGS_D)
CC_FLAGS_T += -Wno-unused-variable


FILES = ArgParser.o ConfigParser.o Pod.o State.o



FILES_D = ArgParser_d.o ConfigParser_d.o Pod_d.o State_d.o


FILES_M = timer_event powerstatus_event process_event public_ip_event
FILES_M += clock_event podmq_event http_event inotify_event
FILES_M += print_mod process_mod processoutput_mod network_mod log_mod
FILES_M += smtp_mod mpd_mod weather_mod dailyword_mod podmq_mod speak_mod
FILES_M += system_mod httpresponse_mod inotifyresponse_mod

CC_FLAGS_M = -g -lpodnet_d -lpodomation_d


all: libpodomation.so 

debug: libpodomation_d.so 



dir:
	mkdir -p bin/

dir_mod:
	mkdir -p Modules/

clean: dir dir_mod
	rm -rf bin
	rm -rf Modules

install:
	cp bin/libpodomation.so /usr/lib/

install_debug: 
	cp bin/libpodomation_d.so /usr/lib/

install_headers:
	rm -rf /usr/include/Podomation/
	mkdir -p /usr/include/Podomation
	find . -type f -name "*.h" -exec install -D {} /usr/include/Podomation/{} \;
	mv /usr/include/Podomation/Src/* /usr/include/Podomation/
	rm -rf /usr/include/Podomation/Src

install_deps:
	pacman -S --noconfirm libmpdclient



server: dir
	$(CC) -o bin/podomation Src/Main.c -lpodomation -lpodnet $(CC_FLAGS)

server_debug: dir
	$(CC) -g -o bin/podomation_d Src/Main.c -lpodomation_d -lpodnet_d $(CC_FLAGS_D)



libpodomation.so: dir $(FILES)
	$(CC) -shared -fPIC -s -o bin/libpodomation.so bin/*.o -pthread -lpodnet $(CC_FLAGS)
	rm bin/*.o

libpodomation_d.so: dir $(FILES_D)
	$(CC) -g -shared -fPIC -o bin/libpodomation_d.so bin/*_d.o -pthread -lpodnet_d $(CC_FLAGS_D)
	rm bin/*.o


modules: dir_mod $(FILES_M)


# EVENT MODULES
clock_event:
	$(CC) -shared -fPIC Src/Modules/ClockEvent.c -o Modules/ClockEvent $(CC_FLAGS_M)

timer_event:
	$(CC) -shared -fPIC Src/Modules/TimerEvent.c -o Modules/TimerEvent $(CC_FLAGS_M)

powerstatus_event:
	$(CC) -shared -fPIC Src/Modules/PowerStatusEvent.c -o Modules/PowerStatusEvent $(CC_FLAGS_M)

process_event:
	$(CC) -shared -fPIC Src/Modules/ProcessEvent.c -o Modules/ProcessEvent $(CC_FLAGS_M)

public_ip_event:
	$(CC) -shared -fPIC Src/Modules/PublicIpEvent.c -o Modules/PublicIpEvent $(CC_FLAGS_M)

podmq_event: 
	$(CC) -shared -fPIC Src/Modules/PodMQEvent.c -o Modules/PodMQEvent -lpodmq_d $(CC_FLAGS_M)

http_event:
	$(CC) -shared -fPIC Src/Modules/HttpEvent.c -o Modules/HttpEvent -lcrock_d $(CC_FLAGS_M)

inotify_event:
	$(CC) -shared -fPIC Src/Modules/InotifyEvent.c -o Modules/InotifyEvent $(CC_FLAGS_M)


# MODULES

print_mod: 
	$(CC) -shared -fPIC Src/Modules/Print.c -o Modules/Print $(CC_FLAGS_M)

process_mod: 
	$(CC) -shared -fPIC Src/Modules/Process.c -o Modules/Process $(CC_FLAGS_M)

processoutput_mod:
	$(CC) -shared -fPIC Src/Modules/ProcessOutput.c -o Modules/ProcessOutput $(CC_FLAGS_M)

network_mod: 
	$(CC) -shared -fPIC Src/Modules/Network.c -o Modules/Network $(CC_FLAGS_M)

log_mod: 
	$(CC) -shared -fPIC Src/Modules/Log.c -o Modules/Log $(CC_FLAGS_M)

smtp_mod:
	$(CC) -shared -fPIC Src/Modules/Smtp.c -o Modules/Smtp $(CC_FLAGS_M)

mpd_mod:
	$(CC) -shared -fPIC Src/Modules/Mpd.c -o Modules/Mpd $(CC_FLAGS_M) -lmpdclient

weather_mod:
	$(CC) -shared -fPIC Src/Modules/Weather.c -o Modules/Weather $(CC_FLAGS_M)

dailyword_mod:
	$(CC) -shared -fPIC Src/Modules/DailyWord.c -o Modules/DailyWord $(CC_FLAGS_M)

podmq_mod:
	$(CC) -shared -fPIC Src/Modules/PodMQ.c -o Modules/PodMQ $(CC_FLAGS_M)

gpio_mod:
	$(CC) -shared -fPIC Src/Modules/Gpio.c -o Modules/Gpio $(CC_FLAGS_M)

speak_mod:
	$(CC) -shared -fPIC Src/Modules/Speak.c -o Modules/Speak $(CC_FLAGS_M)

system_mod:
	$(CC) -shared -fPIC Src/Modules/System.c -o Modules/System $(CC_FLAGS_M)

httpresponse_mod:
	$(CC) -shared -fPIC Src/Modules/HttpResponse.c -o Modules/HttpResponse -lcrock_d $(CC_FLAGS_M)

inotifyresponse_mod:
	$(CC) -shared -fPIC Src/Modules/InotifyResponse.c -o Modules/InotifyResponse -lpodmq_d $(CC_FLAGS_M)


# Components

ArgParser.o:
	$(CC) -fPIC -c -o bin/ArgParser.o Src/Config/ArgParser.c -lpodnet $(CC_FLAGS)

ArgParser_d.o:
	$(CC) -g -fPIC -c -o bin/ArgParser_d.o Src/Config/ArgParser.c -lpodnet_d $(CC_FLAGS_D)


ConfigParser.o:
	$(CC) -fPIC -c -o bin/ConfigParser.o Src/Config/ConfigParser.c -lpodnet $(CC_FLAGS)

ConfigParser_d.o:
	$(CC) -g -fPIC -c -o bin/ConfigParser_d.o Src/Config/ConfigParser.c -lpodnet_d $(CC_FLAGS_D)


Pod.o:
	$(CC) -fPIC -c -o bin/Pod.o Src/Pod/Pod.c -lpodnet $(CC_FLAGS)

Pod_d.o:
	$(CC) -g -fPIC -c -o bin/Pod_d.o Src/Pod/Pod.c -lpodnet_d $(CC_FLAGS_D)


State.o:
	$(CC) -fPIC -c -o bin/State.o Src/State/State.c -lpodnet $(CC_FLAGS)

State_d.o:
	$(CC) -g -fPIC -c -o bin/State_d.o Src/State/State.c -lpodnet_d $(CC_FLAGS_D)

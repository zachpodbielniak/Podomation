#  ____           _                       _   _             
# |  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
# | |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
# |  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
# |_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

# Modular Event-Based Handler In C.
# Copyright (C) 2019 Zach Podbielniak

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License # along with this program.  If not, see <https://www.gnu.org/licenses/>. # along with this program.  If not, see <https://www.gnu.org/licenses/>. 

FROM zachpodbielniak/podmq:latest

# First Build CRock Since We Have A CRock Module.
WORKDIR /crock
RUN pacman -Syy --noconfirm
RUN pacman -S --noconfirm python-requests
RUN git clone https://gitlab.com/zachpodbielniak/CRock
RUN mv CRock/* ./ && rm -rf CRock 
RUN make debug -j2
RUN make install_debug 
RUN make -j2 
RUN make install 
RUN make install_headers

WORKDIR /podomation
COPY . .

RUN make install_deps
RUN make debug -j
RUN make install_debug
RUN make install_headers
RUN make server_debug
RUN make modules -j

CMD ["./bin/podomation_d", "--config", "Config"]
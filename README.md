# Podomation

An event-based response system, focusing on the use of automation.

## License

![AGPLv3](https://www.gnu.org/graphics/agplv3-with-text-162x68.png)

```
 ____           _                       _   _             
|  _ \ ___   __| | ___  _ __ ___   __ _| |_(_) ___  _ __
| |_) / _ \ / _` |/ _ \| '_ ` _ \ / _` | __| |/ _ \| '_ \
|  __/ (_) | (_| | (_) | | | | | | (_| | |_| | (_) | | | |
|_|   \___/ \__,_|\___/|_| |_| |_|\__,_|\__|_|\___/|_| |_|

Modular Event-Based Handler In C.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

Podomation is licensed under the AGPLv3 license. Don't like it? Go else where.


## Donate

Like Podomation? You use it yourself? Why not donate to help make it better! Seriously though, I appreciate any and all donations

+ [PayPal](https://paypal.me/ZPodbielniak)
+ **Bitcoin** - 3C6Fc9WPH54GoVC91Sq4JTWa5C9ijKMA23
+ **Litecoin** - MVjvGjfmp3gkLniBSnreFb3SNEXq1FRxbW
+ **Ethereum** - 0xE58bAEd820308038092F732151c162f530361B59

## Idea

The idea behind this tool is to create PODs. A POD is a Promised Object Dispatch, which is basically an event handler, that has a cause and an effect. The cause is the event in question, and the effect is the dispatched procedure.

## Modular

Podomation is extremely modular. It allows one to write external, loadable modules which can be loaded on certain events, to handle the event.

For example, maybe when a GPIO pin is triggered, we want to send a POST request. We can then have a custom POST module, which then we call.

## Config File

The config file will contain something reminiscent of a programming language. This will allow us to easily specify some form of event, and what function / module to call to handle it.

### Example Config

```
HashTableSize = 8192;

POD timer1 = TimerEvent:New(1000);
timer1->OnTick = Process:ExecuteWritable(echo,timer\1);

POD timer2 = TimerEvent:New(5000);
timer2->OnTick = Print:StringLine(timer2\has\Ticked);

POD timer3 = TimerEvent:New(2000);
timer3->OnTick = Process:ExecuteWritable(/home/zach/bin/scripts/GetTotalBandwidthUsage);

```

This will specify the hash table for defined POD symbols to a size of 8192. Anything smaller is really not recommended. Setting it at least this high severely reduces the size of collisions in the hash table. 

The first POD that is declared (timer1) is a utilizing a new TimerEvent, with a value of 1000. 1000 indicates that the underlying tick time is 1000 milliseconds. The following line specifies the OnTick event, which will make a call to the ExecuteWritable procedure from the Process module. This will call the program echo, with a parameter of 'timer 1'.

The second timer is another new TimerEvent with a different tick time. This one however makes a call to the StringLine procedure form the Print module.

The last timer has a tick time of 2000 milliseconds, and OnTick will call a script in my home folder.

Please note the empty line, as the last line. If you simply ended it on the 'timer3->OnTick' line, the parser will get confused. This is a bug.